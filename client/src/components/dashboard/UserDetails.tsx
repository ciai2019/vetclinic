import React from "react";
import {Col, Image,} from "react-bootstrap";
import default_avatar from './img/default_avatar.png';

const UserDetails = (
    props: {
        name: string,
        email: string,
        phoneNumber: string,
        username?: string,
        picture?: string,
        address?: string,
    }
) => {
    let userPicture = props.picture !== "" ? props.picture : default_avatar;

    return (<>
        <Col xs={4}>
            <Image className="w-100" src={userPicture} roundedCircle/>
        </Col>
        <Col xs={4}>
            <h3>{props.name}</h3>
            <p><b>Username: </b>{props.username}</p>
            <p><b>Email: </b>{props.email}</p>
            <p><b>Address: </b>{props.address}</p>
            <p><b>Phone number: </b>{props.phoneNumber}</p>
        </Col>
    </>);
};

export default UserDetails;