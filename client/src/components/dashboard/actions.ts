import {Action} from "redux";

export const LOAD_VIEW = "LOAD_VIEW";
export interface LoadViewAction extends Action{
        view: string
}

export const loadView = (view: string) => ({
        type: LOAD_VIEW,
        view: view
});


export const SELECT_VET = "SELECT_VET";
export interface SelectVetAction extends Action{
        selectedVetId: number
}

export const selectVet = (vetId: number) => ({
        type: SELECT_VET,
        selectedVetId: vetId
});

export const UNSELECT_VET = "UNSELECT_VET";
export const unselectVet = () => ({
        type: UNSELECT_VET,
});

export const SELECT_VET_APPOINTMENTS = "SELECT_VET_APPOINTMENTS";
export const selectVetAppointments = (vetId: number) => ({
        type: SELECT_VET_APPOINTMENTS,
        selectedVetId: vetId
});