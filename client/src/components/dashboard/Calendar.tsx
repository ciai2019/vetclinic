import React from "react";
import FullCalendar from "@fullcalendar/react";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";

import '@fullcalendar/core/main.css';
import '@fullcalendar/timegrid/main.css';

export interface CalendarEvent {
    title: string,
    start: string,
    end: string
}

const Calendar = (
    props: {
        events: CalendarEvent[],
        handleDateClick?: (arg: any) => void,
        handleEventClick?: (arg: any) => void
        selectable?: boolean
    }
) => {
    return <FullCalendar
        defaultView="timeGridWeek"
        plugins={[timeGridPlugin, interactionPlugin]}
        events={props.events}
        dateClick={props.handleDateClick}
        eventClick={props.handleEventClick}
        selectable={props.selectable}
    />
};

export default Calendar;