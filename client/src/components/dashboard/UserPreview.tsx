import React, {useEffect, useState} from "react";
import {GlobalState} from "../app/App";
import {connect} from "react-redux";
import {Card} from "react-bootstrap";

import avatar from './img/default_avatar.png';
import {getData} from "../../network_utils";

interface UserInfo {
    name: string,
    picture: string,
    email: string,
    phoneNumber: string,
    address: string
}

const ProtoUserPreview = (
    props: {
        userId: string,
        role: string
    }
) => {
    const defaultState: UserInfo = {
        address: "",
        email: "",
        name: "",
        phoneNumber: "",
        picture: ""
    };

    const [userInfo, setUserInfo] = useState<UserInfo>(defaultState);

    useEffect(() => {
        getData<UserInfo>(`api/users/${props.userId}`).then(userInfo => {
            if(userInfo) {
                setUserInfo(userInfo);
            }
        })
    }, [props.userId]);

    // Cut 'ROLE_' and capitalize first letter only
    let displayRole = props.role.substr(5, props.role.length).toLowerCase();
    displayRole = displayRole[0].toUpperCase() + displayRole.slice(1);

    return (
        <>
           <Card.Img variant="top" src={(userInfo.picture.length !== 0) ? userInfo.picture : avatar}/>
           <Card.Body className="text-center">
               <Card.Title>{userInfo.name}</Card.Title>
               <Card.Text>{displayRole}</Card.Text>
           </Card.Body>
        </>
    );
};

const mapStateToProps = (state: GlobalState) => ({userId: state.login.userId, role: state.login.role});
const UserPreview = connect(mapStateToProps)(ProtoUserPreview);

export default UserPreview;