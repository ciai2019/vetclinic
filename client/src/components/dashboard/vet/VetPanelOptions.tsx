import React from 'react';
import {connect} from "react-redux";

import {ListGroup} from 'react-bootstrap';
import {loadView} from "../actions";
import {VIEW_TYPES} from "../UserDashboard";
import {GlobalState} from "../../app/App";


const ProtoVetPanelOptions = (
    props: {
        view: string,
        changeView: (view: string) => void
    }
) => {
    let defaultActiveKey = (props.view !== "") ? "#" + props.view : "#account";

    return(
        <ListGroup variant="flush" defaultActiveKey={defaultActiveKey}>
            <ListGroup.Item action href="#account"
                            onClick={() => {props.changeView(VIEW_TYPES.account)}}>
                Account
            </ListGroup.Item>
            <ListGroup.Item action href="#pets"
                            onClick={() => {props.changeView(VIEW_TYPES.pets)}}>
                Pets
            </ListGroup.Item>
            <ListGroup.Item action href="#schedule"
                            onClick={() => {props.changeView(VIEW_TYPES.schedule)}}>
                Schedule
            </ListGroup.Item>
        </ListGroup>
    );
};

const mapStateToProps = (state: GlobalState) => ({view: state.dashboard.view});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            changeView: (view: string) => { dispatch(loadView(view)) }
        });

const VetPanelOptions = connect(mapStateToProps, mapDispatchToProps)(ProtoVetPanelOptions);
export default VetPanelOptions;