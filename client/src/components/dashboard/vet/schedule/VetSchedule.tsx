import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../../../app/App";
import {unselectVet} from "../../actions";
import {Card} from "react-bootstrap";
import Calendar, {CalendarEvent} from "../../Calendar";
import AppointmentDetailsModal, {AppointmentDetails} from "../../admin/appointments/AppointmentDetailsModal";
import Moment from 'moment';
import {getData} from "../../../../network_utils";


export interface Appointment {
    id: number,
    timeSlot: {
        startDate: string,
        finishDate: string
    },
    completed: boolean,
    vetId: number,
    clientId: number,
    petId: number,
    description: string
}

export interface AppointmentEvent extends CalendarEvent{
    extendedProps: {
        appointmentId: number,
        vetId: number,
        clientId: number,
        petId: number
    }
}

interface AvailableSlot {
    id: number,
    startDate: string,
    finishDate: string,
    vetId: number
}

interface AvailableSlotEvent extends CalendarEvent{
    extendedProps: {
        slotId: number,
        vetId: number
    }
}

const ProtoSchedule = (
    props: {
        role: string,
        userId: string,
    }
) => {

    const [appointmentsList, setAppointmentsList] = useState([] as AppointmentEvent[]);
    const [updateList, setUpdateList] = useState(true);
    const [timeSlots, setTimeSlots] = useState([] as AvailableSlotEvent[]);

    useEffect(() => {
        function fetchAppointments() {
            let url = `/api/vets/${props.userId}/appointments`;

            getData<Appointment[]>(url).then(appointments => {

                if (appointments) {
                    setAppointmentsList(appointments.map(apt => ({
                        title: "Appointment",
                        start: apt.timeSlot.startDate,
                        end: apt.timeSlot.finishDate,
                        extendedProps: {
                            appointmentId: apt.id,
                            vetId: apt.vetId,
                            clientId: apt.clientId,
                            petId: apt.petId,
                            isCompleted: apt.completed,
                            description: apt.description
                        },
                        color: apt.completed ? "#29e65b" : "#ffe53b"
                    })));
                }
            });
        }

        function fetchAvailableSlots() {
                getData<AvailableSlot[]>(`/api/vets/${props.userId}/timeslots/available`)
                    .then(slots => {

                        if(slots) {
                            setTimeSlots(slots.map(slot => ({
                                title: "Available Slot",
                                start: slot.startDate,
                                end: slot.finishDate,
                                extendedProps: {
                                    slotId: slot.id,
                                    vetId: slot.vetId
                                },
                                color: "#0174DF"

                            })));
                        }
                    });
        }

        //setAllSlots([] as CalendarEvent[]);
        fetchAppointments();
        fetchAvailableSlots();
        if(updateList){
            setUpdateList(false)
        }

    }, [props.userId, props.role, updateList]);

    const [showDetailsModal, setShowDetailsModal] = useState(false);
    const [aptDetails, setAptDetails] = useState<AppointmentDetails | null>(null);

    function handleSlotClick(args: any) {
        if(args.event.extendedProps.appointmentId){
            setShowDetailsModal(true);
            console.log(args.event);

            setAptDetails({
                id: args.event.extendedProps.appointmentId,
                start: Moment(args.event.start).format("YYYY-MM-DD HH:mm:ss").toString(),
                end: Moment(args.event.end).format("YYYY-MM-DD HH:mm:ss").toString(),
                vetId: args.event.extendedProps.vetId,
                clientId: args.event.extendedProps.clientId,
                petId: args.event.extendedProps.petId,
                isCompleted: args.event.extendedProps.isCompleted,
                description: args.event.extendedProps.description
            });
        }
    }

    const handleDetailsModalClose = () => {
        setShowDetailsModal(false);
        setUpdateList(true);
    };

    let allSlots = (appointmentsList as CalendarEvent[]).concat(timeSlots as CalendarEvent[]);

    return (<>
        <Card>
            <Card.Header className='d-flex justify-content-between align-items-center'>
                {"Schedule"}
            </Card.Header>
            <Card.Body>
                <Calendar events={allSlots} handleEventClick={handleSlotClick}/>
            </Card.Body>
        </Card>
        {aptDetails &&
        <AppointmentDetailsModal showModal={showDetailsModal} handleClose={handleDetailsModalClose}
                                 details={aptDetails}/>
        }
    </>);
};

const mapStateToProps = (state: GlobalState) => ({
    selectedVetId: state.dashboard.appointmentsList.selectedVetId,
    userId: state.login.userId,
    role: state.login.role
});

const mapDispatchToProps =
    (dispatch: any) => ({
        unselectVet: () => {dispatch(unselectVet())}
    });

const VetSchedule = connect(mapStateToProps, mapDispatchToProps)(ProtoSchedule);
export default VetSchedule;