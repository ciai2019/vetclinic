import React, {useEffect, useState} from "react";
import {Col, Container, Modal, Row, Image} from "react-bootstrap";
//import http from '../../../http';
import axios from 'axios';

interface Pet {
    id: number,
    name: string,
    species: string,
    age: number,
    picture: string,
    physicalDescription: string,
    notes: string,
    medicalRecord: string
}

const PetDetails = (
    props: {
        petId: number | null,
        showModal: boolean,
        handleClose: () => void
    }
) => {

    const initialState: Pet = {
        id: -1,
        name: "",
        species: "",
        age: 0,
        picture: "",
        physicalDescription: "",
        notes: "",
        medicalRecord: ""
    };
    const [petDetails, setPetDetails] = useState<Pet>(initialState);

    useEffect(() => {
        async function fetchPetDetails(){
            const result = await axios.get<Pet>(`/api/pets/${props.petId}`);
            setPetDetails(result.data);
        }

        if(props.petId){
            fetchPetDetails();
        }
    }, [props.petId]);

    return(
        <Modal
            show={props.showModal}
            onHide={props.handleClose}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Pet Details
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col xs={4}>
                            <Image className="w-100" src={petDetails.picture} roundedCircle/>
                        </Col>
                        <Col xs={8}>
                            <h3>{petDetails.name}</h3>
                            <p>{petDetails.species}</p>
                            <p>{petDetails.age}</p>
                            <p>{petDetails.physicalDescription}</p>
                            <p>{petDetails.notes}</p>
                            <p>{petDetails.medicalRecord}</p>
                        </Col>
                    </Row>

                </Container>
            </Modal.Body>
        </Modal>
    );
};

export default PetDetails;