import React from "react";
import {Row, Col} from 'react-bootstrap';
import {connect} from "react-redux";
import {Redirect} from 'react-router-dom';

import {GlobalState} from "../app/App";
import SidePanel from "./SidePanel";
import EmployeeList from "./admin/employees/EmployeeList";
import PetList from "./client/pets/PetList";

import './styles/UserDashboard.css'
import AppointmentList, {AppointmentsListState} from "./admin/appointments/AppointmentList";
import VetSchedule from "./vet/schedule/VetSchedule";


export const ROLE_ADMIN = "ROLE_ADMIN";
export const ROLE_VET = "ROLE_VET";
export const ROLE_CLIENT = "ROLE_CLIENT";

export const VIEW_TYPES = {
    account: "account",
    employees: "employees",
    clients: "clients",
    pets: "pets",
    appointments: "appointments",
    schedule:"schedule"
};

export interface DashboardState {
    view: string,
    appointmentsList: AppointmentsListState
}


const ProtoUserDashboard = (
    props: {
        isLoggedIn: boolean,
        view: string
    }
) => {

    let dashboardView;

    switch (props.view) {
        case VIEW_TYPES.employees:
            dashboardView = <EmployeeList/>;
            break;
        case VIEW_TYPES.pets:
            dashboardView = <PetList/>;
            break;

        case VIEW_TYPES.appointments:
            dashboardView = <AppointmentList/>;
            break;
        case VIEW_TYPES.schedule:
            dashboardView = <VetSchedule/>;
            break;
    }

    let page = (
        <Row className="dashboard">
            <Col xs={2}>
                <SidePanel/>
            </Col>
            <Col xs={10}>
                {dashboardView}
            </Col>
        </Row>
    );

    return <>{props.isLoggedIn ? page: <Redirect to={"/"}/>}</>;
};

const mapStateToProps = (state: GlobalState) => ({isLoggedIn: state.login.isLoggedIn, view: state.dashboard.view});

const UserDashboard = connect(mapStateToProps)(ProtoUserDashboard);

export default UserDashboard;