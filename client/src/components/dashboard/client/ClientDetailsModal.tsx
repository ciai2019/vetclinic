import React, {useEffect, useState} from "react";
import {Modal} from "react-bootstrap";
import UserDetailsModal, {User} from "../UserDetailsModal";
import {getData} from "../../../network_utils";

const ClientDetailsModal = (
    props: {
        clientId: number,
        showModal: boolean,
        handleClose: () => void
    }
) => {
    const [client, setClient] = useState<User | null>(null);

    useEffect(() => {
        getData<User>(`/api/clients/${props.clientId}`).then(client => {
            if(client) {
                setClient(client);
            }
        })
    }, [props.clientId]);


    return (
        <Modal
            show={props.showModal}
            onHide={() => props.handleClose()}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            { client &&  <UserDetailsModal title={"Client details"} user={client}/>}
        </Modal>
    );
};

export default ClientDetailsModal;