import React, {useState} from "react";
import {Modal, Form, Button, FormControlProps} from "react-bootstrap";
import {postData} from "../../../../network_utils";

interface NewPet {
    name?: string,
    species?: string,
    age?: string,
    picture?: string,
    physicalDescription?: string,
    notes?: string,
    medicalRecord?: string
    ownerId:string
}

const CreatePetForm = (
    props: {
        clientId:string,
        showModal: boolean,
        handleClose: () => void
    }
) => {

    const initialStateNewPet: NewPet = {
        name: "",
        species: "",
        age: "",
        picture: "",
        physicalDescription: "",
        notes: "",
        medicalRecord: "",
        ownerId: props.clientId
    };
    const [newPetDetails, setNewPetDetails] = useState<NewPet>(initialStateNewPet);

    function handleSubmit() {
        postData("/api/pets", newPetDetails).then(response => {
            if(response.status === 200) {
                props.handleClose();
                alert("Successfully created a pet");
                setNewPetDetails(initialStateNewPet);
            }
        }).catch(error => {
            console.log(error.response.message);
            alert("Error creating a pet");
        });
    }


    let handleFieldChange = (e: React.FormEvent<HTMLInputElement>, field: string) => {
      switch (field) {
          case "name" : setNewPetDetails({...newPetDetails, name: e.currentTarget.value as string}); break;
          case "species" : setNewPetDetails({...newPetDetails, species: e.currentTarget.value as string}); break;
          case "age" : setNewPetDetails({...newPetDetails, age: e.currentTarget.value as string}); break;
          case "picture" : setNewPetDetails({...newPetDetails, picture: e.currentTarget.value as string}); break;
          case "physicalDescription" :    setNewPetDetails({...newPetDetails, physicalDescription: e.currentTarget.value as string}); break;
          case "notes": setNewPetDetails({...newPetDetails, notes: e.currentTarget.value as string}); break;
          case "medicalRecord" :  setNewPetDetails({...newPetDetails, medicalRecord: e.currentTarget.value as string}); break;
      }
    };

    let modalForm = (<>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                Create New Pet
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formPetName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control required type="name" placeholder="Enter Name"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "name")} />
                </Form.Group>
                <Form.Group controlId="formPetSpecies">
                    <Form.Label>Species</Form.Label>
                    <Form.Control type="name" placeholder="Enter Species"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "species")}/>
                </Form.Group>
                <Form.Group controlId="formPetAge">
                    <Form.Label>Age</Form.Label>
                    <Form.Control type="name" placeholder="Enter Age"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "age")}/>
                    <Form.Text className="text-muted">
                        If you dont know the age of your pet insert -1
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="formPetPicture">
                    <Form.Label>Picture</Form.Label>
                    <Form.Control type="name" placeholder="Enter Picture"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "picture")}/>
                </Form.Group>
                <Form.Group controlId="formPetPhysicalDescription">
                    <Form.Label>Physical Description</Form.Label>
                    <Form.Control type="name" placeholder="Enter Physical Description"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "physicalDescription")}/>
                </Form.Group>
                <Form.Group controlId="formPetNotes">
                    <Form.Label>Notes</Form.Label>
                    <Form.Control type="name" placeholder="Enter Notes"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "notes")}/>
                </Form.Group>
                <Form.Group controlId="formPetMedicalRecord">
                    <Form.Label>MedicalRecord</Form.Label>
                    <Form.Control type="name" placeholder="Enter Medical Record"
                                  onChange={(e: React.FormEvent<HTMLInputElement>) => handleFieldChange(e, "medicalRecord")}/>
                </Form.Group>
                <Button type="submit">Submit</Button>
            </Form>
        </Modal.Body>
    </>);

    return(
        <Modal
            show={props.showModal}
            onHide={props.handleClose}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            {modalForm}
        </Modal>
    );
};

export default CreatePetForm;