import React, {useEffect, useState} from "react";
import {Col, Container, Modal, Row, Image} from "react-bootstrap";
import pet_avatar from "../../img/default_pet_avatar.png";
import {getData} from "../../../../network_utils";

export interface Pet {
    id: number,
    name: string,
    species: string,
    age: number,
    picture: string,
    physicalDescription: string,
    notes: string,
    medicalRecord: string
}

const PetDetailsModal = (
    props: {
        petId: number | null,
        showModal: boolean,
        handleClose: () => void
    }
) => {

    const initialState: Pet = {
        id: -1,
        name: "",
        species: "",
        age: 0,
        picture: "",
        physicalDescription: "",
        notes: "",
        medicalRecord: ""
    };
    const [petDetails, setPetDetails] = useState<Pet>(initialState);

    useEffect(() => {
        if(props.showModal && props.petId){
            getData<Pet>(`/api/pets/${props.petId}`).then(pet => {
                if(pet) {
                    setPetDetails(pet);
                }
            })
        }
    }, [props.petId, props.showModal]);

    let modalView = (<>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                Pet Details
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Container>
                <Row>
                    <Col xs={4}>
                    <Image className="w-100" src={(petDetails.picture.length !== 0) ? petDetails.picture : pet_avatar}roundedCircle/>
                    </Col>
                    <Col xs={4}>
                        <h3>{petDetails.name}</h3>
                        <p><b> Species: </b>{(petDetails.species.length !== 0) ? petDetails.species : "not specified"}</p>
                        <p><b> Age: </b>{ (petDetails.age >= 0) ? petDetails.age : "not specified"}</p>
                        <p><b> Physical Description: </b>{(petDetails.physicalDescription.length !== 0) ? petDetails.physicalDescription : "none"}</p>
                        <p><b> Notes: </b>{(petDetails.notes.length !== 0) ? petDetails.notes : "none"}</p>
                        <p><b> Medical Record: </b>{(petDetails.medicalRecord.length !== 0) ? petDetails.medicalRecord : "none"}</p>
                    </Col>
                </Row>
            </Container>
        </Modal.Body>
    </>);

    return(
        <Modal
            show={props.showModal}
            onHide={() => {props.handleClose()}}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            {modalView}
        </Modal>
    );
};

export default PetDetailsModal;