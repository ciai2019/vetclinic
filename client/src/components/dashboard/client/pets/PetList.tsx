import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Card, ListGroup, Tab, Form, FormControlProps, Image, Button} from "react-bootstrap";
import {GlobalState} from "../../../app/App";
import PetDetailsModal from "./PetDetailsModal";
import CreatePetForm from "./CreatePetForm";
import '../styles/PetList.css'
import pet_avatar from "../../img/default_pet_avatar.png";
import {ROLE_ADMIN, ROLE_CLIENT, ROLE_VET} from "../../UserDashboard";
import {getData} from "../../../../network_utils";

interface Pet {
    id: number,
    name: string,
    species: string,
    age :number,
    picture: string,
    physicalDescription: string,
    notes: string,
    medicalRecord: string
}

const ProtoPetList = (
    props: {
        isLoggedIn: boolean,
        userId: string,
        role:string
    }
) => {
    const [petList, setPetList] = useState([] as Pet[]);
    const [filter, setFilter] = useState("");

    let handleFilterChange = (e: React.FormEvent<HTMLInputElement>) => {
        setUpdatePetList(true);
        setFilter(e.currentTarget.value as string);
    };

    let filterInput = <Form.Control type="text" placeholder="Filter by name"
                                    onChange={(e: React.FormEvent<HTMLInputElement>) => {handleFilterChange(e)}}/>;

    const [showModalNewPet, setShowModalNewPet] = useState(false);
    const [updatePetList, setUpdatePetList] = useState(true);

    const handleCloseModalNewPet = () => {
        setShowModalNewPet(false);
        setUpdatePetList(true);
    };

    const [showModal, setShowModal] = useState(false);
    const handleCloseModal = () => setShowModal(false);

    const [selectedPetId, setSelectedPetId] = useState<number | null>(null);

    function handlePetClick(petId: number){
        setSelectedPetId(petId);
        setShowModal(true);
    }
    function handleCreatePetClick(){
        setShowModalNewPet(true);
    }
    useEffect(() => {
        let url = (props.role === ROLE_ADMIN || props.role === ROLE_VET ) ? `/api/pets` : `/api/clients/${props.userId}/pets`;
        getData<Pet[]>(url, {
            name: filter
        }).then(pets => {
            if(pets) {
                setPetList(pets);
            }
        });

        if(updatePetList){
            setUpdatePetList(false);
        }
    }, [filter, props.userId, updatePetList, props.role]);

    let pets = petList.map(pet =>
        <ListGroup.Item action key={ pet.id } onClick={() => { handlePetClick(pet.id) }}>
            <div className="pet-img-container d-inline-block">
                <Image src={(pet.picture.length !== 0) ? pet.picture : pet_avatar} fluid roundedCircle/>
            </div>
            {pet.name}
        </ListGroup.Item>
    );

    return (<>
        <Tab.Container defaultActiveKey="pets">
            <Card>
                <Card.Header className='d-flex justify-content-between align-items-center'>
                    My Pets
                    {props.role===ROLE_CLIENT && <Button onClick={handleCreatePetClick}>New Pet</Button> }
                </Card.Header>
                <Card.Body>
                    <Tab.Content>
                        {props.isLoggedIn && filterInput}
                        <Tab.Pane eventKey="pets">
                            <ListGroup variant="flush">
                                {pets}
                            </ListGroup>
                        </Tab.Pane>
                    </Tab.Content>
                </Card.Body>
            </Card>
        </Tab.Container>
        <PetDetailsModal handleClose={handleCloseModal} showModal={showModal} petId={selectedPetId}/>
        <CreatePetForm clientId={props.userId} showModal={showModalNewPet} handleClose={handleCloseModalNewPet}/>
        </>
    );
};

const mapStateToProps = (state:GlobalState) => ({isLoggedIn: state.login.isLoggedIn, userId: state.login.userId , role: state.login.role});

const PetList = connect(mapStateToProps)(ProtoPetList);
export default PetList;