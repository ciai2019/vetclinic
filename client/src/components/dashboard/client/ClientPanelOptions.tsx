import React from 'react';
import {connect} from "react-redux";
import {ListGroup} from 'react-bootstrap';
import {loadView} from "../actions";
import {VIEW_TYPES} from "../UserDashboard";
import {GlobalState} from "../../app/App";


const ProtoClientPanelOptions = (
    props: {
        view: string,
        changeView: (view: string) => void
    }
) => {
    let defaultActiveKey = (props.view !== "") ? "#" + props.view : "#my-details";

    return(
        <ListGroup variant="flush" defaultActiveKey={defaultActiveKey}>
            <ListGroup.Item action href="#my-details"
                            onClick={() => {props.changeView(VIEW_TYPES.clients)}}>
                My Details
            </ListGroup.Item>
            <ListGroup.Item action href="#pets"
                            onClick={() => {props.changeView(VIEW_TYPES.pets)}}>
                Pets
            </ListGroup.Item>
            <ListGroup.Item action href="#appointments"
                            onClick={() => {props.changeView(VIEW_TYPES.appointments)}}>
                Appointments
            </ListGroup.Item>
        </ListGroup>
    );
};

const mapStateToProps = (state: GlobalState) => ({view: state.dashboard.view});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            changeView: (view: string) => { dispatch(loadView(view)) }
        });

const ClientPanelOptions = connect(mapStateToProps, mapDispatchToProps)(ProtoClientPanelOptions);
export default ClientPanelOptions;