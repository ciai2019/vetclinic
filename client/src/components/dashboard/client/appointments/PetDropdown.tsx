import React, {useState} from "react";
import {Button, Dropdown, FormControl, Image} from "react-bootstrap";

import {Pet} from "../pets/PetDetailsModal";

import defaultPetAvatar from '../../img/default_pet_avatar.png';


const PetDropdownToggle = React.forwardRef<any, any>(({ children, onClick }, ref) => (
    <Button
        variant="primary"
        ref={ref}
        onClick={(e: any) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        {children}
        &#x25bc;
    </Button>
));

const PetDropdownMenu = React.forwardRef<any, any>(({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
    const [value, setValue] = useState("");

    return (
        <div
            ref={ref}
            style={style}
            className={className}
            aria-labelledby={labeledBy}
        >
            <FormControl
                autoFocus
                className="mx-3 my-2 w-auto"
                placeholder="Type to filter..."
                onChange={(e: any) => setValue(e.target.value)}
                value={value}
            />
            <ul className="list-unstyled">
                {React.Children.toArray(children).filter(
                    child =>
                        !value || child.props.children[1].toLowerCase().startsWith(value),
                )}
            </ul>
        </div>
    );
});

const PetDropdown = (
    props: {
        pets: Pet[],
        onSelectPet: (petId: number) => void
    }
) => {

    const [selectedPet, setSelectedPet] = useState<Pet | null>(null);

    function handleSelectPet(pet: Pet) {
        setSelectedPet(pet);
        console.log(pet.id);
        props.onSelectPet(pet.id);
    }

    let dropdownItems = props.pets.map((pet, index) => (
        <Dropdown.Item key={index}  onClick={() => handleSelectPet(pet)}>
            <div className="employee-img-container d-inline-block">
                <Image src={pet.picture} fluid roundedCircle/>
            </div>
            {pet.name}
        </Dropdown.Item>
    ));

    let toggleContent: any = "Select pet";

    if(selectedPet) {

        let picture = selectedPet.picture !== "" ? selectedPet.picture : defaultPetAvatar;
        toggleContent = <>
            <div className="employee-img-container d-inline-block">
                <Image src={picture} fluid roundedCircle/>
            </div>
            {selectedPet.name}
        </>;
    }

    return (
        <Dropdown>
            <Dropdown.Toggle as={PetDropdownToggle} id="dropdown-custom-components">
                {toggleContent}
            </Dropdown.Toggle>
            <Dropdown.Menu as={PetDropdownMenu}>
                {dropdownItems}
            </Dropdown.Menu>
        </Dropdown>
    );
};

export default PetDropdown;