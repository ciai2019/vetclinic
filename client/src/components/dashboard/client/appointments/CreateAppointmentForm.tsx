import React, {useEffect, useState} from "react";
import {Container, Modal} from "react-bootstrap";
import Calendar, {CalendarEvent} from "../../Calendar";
import SelectPetModal from "./SelectPetModal";
import {getData} from "../../../../network_utils";


interface AvailableSlot {
    id: number,
    startDate: string,
    finishDate: string,
    vetId: number
}

interface AvailableSlotEvent extends CalendarEvent{
    extendedProps: {
        slotId: number,
        vetId: number
    }
}

const CreateAppointmentForm = (
    props: {
        clientId:string,
        showModal: boolean,
        handleClose: () => void
    }
) => {
    const [timeSlots, setTimeSlots] = useState([] as AvailableSlotEvent[]);

    useEffect(() => {
        if(props.showModal) {
            getData<AvailableSlot[]>("/api/timeslots/available").then(slots => {
                if(slots) {
                    setTimeSlots(slots.map(slot => ({
                        title: "Slot",
                        start: slot.startDate,
                        end: slot.finishDate,
                        extendedProps: {
                            slotId: slot.id,
                            vetId: slot.vetId
                        }
                    })));
                }
            })
        }

    }, [props.showModal]);

    const [showPetSelectForm, setShowPetSelectForm] = useState(false);
    const [slotId, setSlotId] = useState<string | null>(null);
    const [vetId, setVetId] = useState<string | null>(null);

    function handleSlotClick(args: any) {
        setSlotId(args.event.extendedProps.slotId.toString());
        setVetId(args.event.extendedProps.vetId.toString());

        setShowPetSelectForm(true);
    }

    function onSelectPetSubmit() {
        setShowPetSelectForm(false);
        props.handleClose();
    }

    let modalForm = (<>
        <Modal.Header closeButton>
            <Modal.Title>
                Available slots
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Container>
                <Calendar events={timeSlots} handleEventClick={handleSlotClick}/>
            </Container>
        </Modal.Body>
        <SelectPetModal clientId={props.clientId} showModal={showPetSelectForm} onSubmit={onSelectPetSubmit} slotId={slotId} vetId={vetId}/>
    </>);

    return(
        <Modal
            show={props.showModal}
            onHide={props.handleClose}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            {modalForm}
        </Modal>
    );
};

export default CreateAppointmentForm;