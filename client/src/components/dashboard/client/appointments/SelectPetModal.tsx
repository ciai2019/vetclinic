import React, {useEffect, useState} from "react";
import {Button, Container, Modal} from "react-bootstrap";
import PetDropdown from "./PetDropdown";
import {Pet} from "../pets/PetDetailsModal";
import {getData, postData} from "../../../../network_utils";

interface NewAppointment {
    vetId: string,
    petId: string,
    slotId: string,
}

const SelectPetModal = (
    props: {
        clientId: string,
        vetId: string | null,
        slotId: string | null,
        showModal: boolean,
        onSubmit: () => void
    }
) => {
    const [clientPets, setClientPets] = useState([] as Pet[]);
    const [newAppointment, setNewAppointment] = useState<NewAppointment | null>(null);

    useEffect(() => {
        if(props.showModal) {
            getData<Pet[]>(`/api/clients/${props.clientId}/pets`).then(pets => {
                if(pets) {
                    setClientPets(pets);
                }
            })
        }

        if(newAppointment) {
            postData("/api/appointments", newAppointment).then(response => {
                if(response.status === 200) {
                    setNewAppointment(null);
                    props.onSubmit();
                }
            }).catch(error => {
                console.log(error.response.data);
            });
        }
    }, [props.showModal, props.clientId, newAppointment]);

    const [selectedPetId, setSelectedPetId] = useState(-1);
    function handleSelectPet(petId: number) {
        setSelectedPetId(petId);
    }

    function onSubmitHandler() {
        if(props.vetId && props.slotId) {
            setNewAppointment({
                petId: selectedPetId.toString(),
                vetId: props.vetId,
                slotId: props.slotId
            });
        }
    }

    return (
        <Modal
            show={props.showModal}
            onHide={() => props.onSubmit()}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Select pet
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <PetDropdown pets={clientPets} onSelectPet={handleSelectPet}/>
                    <Button variant="primary" block disabled={selectedPetId === -1} onClick={onSubmitHandler}>Submit</Button>
                </Container>
            </Modal.Body>
        </Modal>
    );
};

export default SelectPetModal;