import {Action, combineReducers} from "redux";
import {LOAD_VIEW, LoadViewAction, SELECT_VET, SELECT_VET_APPOINTMENTS, SelectVetAction, UNSELECT_VET} from "./actions";
import {LOG_OUT} from "../login/actions";
import {VIEW_TYPES} from "./UserDashboard";

function getCurrentView() {
    let view = localStorage.getItem("current-dashboard-view");
    return (view == null) ? "" : view;
}

function viewReducer (state= getCurrentView(), action: Action){
    switch (action.type) {
        case LOAD_VIEW:
            let view = (action as LoadViewAction).view;
            localStorage.setItem("current-dashboard-view", view);

            return view;

        case LOG_OUT:
            localStorage.removeItem("current-dashboard-view");
            return "";

        case SELECT_VET_APPOINTMENTS:
            localStorage.setItem("current-dashboard-view", VIEW_TYPES.appointments);
            return VIEW_TYPES.appointments;

        default:
            return state;
    }
}

function getSelectedVetId(){
    let vetId = localStorage.getItem("selected-vet-id");
    return (vetId == null) ? null : +vetId;
}

const initialAppointmentListState= {
    selectedVetId: getSelectedVetId()
};

function appointmentListReducer (state = initialAppointmentListState, action: Action){
    switch (action.type) {
        case SELECT_VET:
        case SELECT_VET_APPOINTMENTS:
            let vetId = (action as SelectVetAction).selectedVetId;
            localStorage.setItem("selected-vet-id", vetId.toString());

            return {...state, selectedVetId: vetId};

        case UNSELECT_VET:
            localStorage.removeItem("selected-vet-id");
            return {...state, selectedVetId: null};

        default:
            return state;
    }
}

const dashboardReducer = combineReducers({
    view: viewReducer,
    appointmentsList: appointmentListReducer
});

export default dashboardReducer;