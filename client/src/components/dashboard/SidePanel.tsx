import React from 'react';
import {Card} from 'react-bootstrap';
import {connect} from "react-redux";

import {GlobalState} from "../app/App";
import {ROLE_ADMIN, ROLE_CLIENT, ROLE_VET} from "./UserDashboard";
import UserPreview from "./UserPreview";

import AdminPanelOptions from "./admin/AdminPanelOptions";
import LoginLogout from "../login/LoginLogout";
import VetPanelOptions from "./vet/VetPanelOptions";
import ClientPanelOptions from "./client/ClientPanelOptions";

const ProtoSidePanel = (
    props: {
        role: string
    }
) => {

    let panelOptions;

    switch (props.role) {
        case ROLE_ADMIN:
            panelOptions = <AdminPanelOptions/>;
            break;

        case ROLE_VET:
            panelOptions = <VetPanelOptions/>;
            break;

        case ROLE_CLIENT:
            panelOptions=<ClientPanelOptions/>
            break;
    }

    return (
        <Card className="shadow-sm">
            <UserPreview/>
            {panelOptions}
            <Card.Body>
                <LoginLogout/>
            </Card.Body>
        </Card>
    );
};

const mapStateToProps = (state: GlobalState) => ({role: state.login.role});
const SidePanel = connect(mapStateToProps)(ProtoSidePanel);

export default SidePanel;
