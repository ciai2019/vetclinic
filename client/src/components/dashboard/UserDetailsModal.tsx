import React from "react";
import {Container, Modal, Row} from "react-bootstrap";

import UserDetails from "./UserDetails";

export interface User{
    name: string,
    username: string,
    email: string,
    picture: string,
    address: string,
    phoneNumber: string
}

const UserDetailsModal = (
    props: {
        user: User,
        title?: string,
        children?: React.ReactNode
    }
) => {
    let title = (props.title != null) ? props.title : "User Details";

    return (<>
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                {title}
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Container>
                <Row>
                    <UserDetails
                        name={props.user.name}
                        username={props.user.username}
                        picture={props.user.picture}
                        email={props.user.email}
                        address={props.user.address}
                        phoneNumber={props.user.phoneNumber}
                    />
                    {props.children}
                </Row>
            </Container>
        </Modal.Body>
    </>);
};

export default UserDetailsModal;

