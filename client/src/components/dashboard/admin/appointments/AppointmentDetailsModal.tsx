import React, {useState} from "react";
import {Button, Col, Modal, Row, Form, FormControlProps} from "react-bootstrap";
import axios from "axios";
import PetDetailsModal from "../../client/pets/PetDetailsModal";
import ClientDetailsModal from "../../client/ClientDetailsModal";
import {GlobalState} from "../../../app/App";
import {connect} from "react-redux";
import {ROLE_VET} from "../../UserDashboard";

export interface AppointmentDetails {
    id: number,
    start: string,
    end: string,
    vetId: number,
    clientId: number,
    petId: number,
    isCompleted:boolean,
    description: string
}

const ProtoAppointmentDetailsModal = (
    props: {
        role: string,
        showModal: boolean,
        handleClose: () => void,
        details: AppointmentDetails
    }
) => {
    const [showPetModal, setShowPetModal] = useState(false);
    const [showClientModal, setShowClientModal] = useState(false);
    const [description, setDescription] = useState("");
    let handleDescriptionChange = (e: React.FormEvent<HTMLInputElement>) =>
        setDescription(e.currentTarget.value as string);

    async function completeAppointment() {
        try {
            const response = await axios.post(`/api/vets/${props.details.vetId}/appointments/completeAppointment`, {
                id: props.details.id,
                description: description
            });
            if(response.status === 200){
                alert("Successfully completed an appointment");
                props.handleClose();
            }
        }catch (e) {
            console.log(e);
        }
    }


    const handleCloseDetailsModal = () => {
        setShowClientModal(false);
        setShowPetModal(false);
    };

    function handlePetBtnClick() {
        setShowPetModal(true);
    }

    function handleClientBtnClick() {
        setShowClientModal(true);
    }

    function handleCloseModal() {
        props.handleClose();
    }

    let title = props.details.isCompleted ? "Appointment details (completed)" : "Appointment details";

    return (
        <Modal
            show={props.showModal}
            onHide={handleCloseModal}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Col xs={6}>
                        <p><b>Start: </b>{props.details.start}</p>
                    </Col>
                    <Col xs={6}>
                        <p><b>End: </b>{props.details.end}</p>
                    </Col>
                </Row>
                {
                    props.details.isCompleted &&
                    <Row>
                        <Col xs={12}>
                            <p><b>Description: </b>{props.details.description}</p>
                        </Col>
                    </Row>
                }
                <Row>
                    <Col xs={6}>
                        <Button variant="primary" size="lg" block onClick={handlePetBtnClick}>Go to pet</Button>
                    </Col>
                    <Col xs={6}>
                        <Button variant="primary" size="lg" block onClick={handleClientBtnClick}>Go to client</Button>
                    </Col>
                </Row>

                {
                    !props.details.isCompleted && (props.role === ROLE_VET) && <>
                    <Row>
                        <Col xs={12}>
                            <Form.Label>Description</Form.Label>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={6}>
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                <Form.Control as="textarea" rows="1" onChange={handleDescriptionChange} />
                            </Form.Group>
                        </Col>
                        <Col xs={6}>
                            <Button variant="primary" size="lg" block onClick={completeAppointment}>Mark as completed</Button>
                        </Col>
                    </Row>
                </>}
            </Modal.Body>
            <PetDetailsModal handleClose={handleCloseDetailsModal} showModal={showPetModal} petId={props.details.petId}/>
            <ClientDetailsModal clientId={props.details.clientId} showModal={showClientModal} handleClose={handleCloseDetailsModal}/>
        </Modal>
         );
    };

const mapStateToProps = (state: GlobalState) => ({role: state.login.role});
const AppointmentDetailsModal = connect(mapStateToProps)(ProtoAppointmentDetailsModal);

export default AppointmentDetailsModal;