import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../../../app/App";
import {unselectVet} from "../../actions";
import {Button, Card} from "react-bootstrap";
import Calendar, {CalendarEvent} from "../../Calendar";
import VetDropdown from "../schedule/VetDropdown";
import {Vet} from "../employees/VetDetailsModal";
import {ROLE_ADMIN, ROLE_CLIENT, ROLE_VET} from "../../UserDashboard";
import AppointmentDetailsModal, {AppointmentDetails} from "./AppointmentDetailsModal";
import Moment from 'moment';
import CreateAppointmentForm from "../../client/appointments/CreateAppointmentForm";
import {getData} from "../../../../network_utils";

export interface AppointmentsListState {
    selectedVetId: number
}

export interface Appointment {
    id: number,
    timeSlot: {
        startDate: string,
        finishDate: string
    },
    completed: boolean,
    vetId: number,
    clientId: number,
    petId: number,
    description: string
}

export interface AppointmentEvent extends CalendarEvent{
    extendedProps: {
        appointmentId: number,
        vetId: number,
        clientId: number,
        petId: number
    }
}

const ProtoAppointmentList = (
    props: {
        selectedVetId: number | null,
        role: string,
        userId: string,
        unselectVet: () => void
    }
) => {

    const [appointmentsList, setAppointmentsList] = useState([] as AppointmentEvent[]);
    const [updateList, setUpdateList] = useState(true);

    const [vets, setVets] = useState([] as Vet[]);

    useEffect(() => {
        function fetchAppointments() {
           let url;
           switch(props.role){
               case ROLE_ADMIN:
                   url = (props.selectedVetId != null) ?
                       `/api/vets/${props.selectedVetId}/appointments` : "/api/appointments";
                   break;
               case ROLE_VET:
                   url = `/api/vets/${props.userId}/appointments`;
                   break;

               case ROLE_CLIENT:
                   url = `/api/clients/${props.userId}/appointments`;
                   break;
           }

           if(url !== undefined) {
               getData<Appointment[]>(url).then(appointments => {
                   if(appointments) {
                       console.log(appointments);
                       setAppointmentsList(appointments.map(apt => ({
                           title: "Appointment",
                           start: apt.timeSlot.startDate,
                           end: apt.timeSlot.finishDate,
                           extendedProps: {
                               appointmentId: apt.id,
                               vetId: apt.vetId,
                               clientId: apt.clientId,
                               petId: apt.petId,
                               isCompleted: apt.completed,
                               description: apt.description
                           },
                           color: apt.completed ? "#29e65b" : "#ffe53b"
                       })));
                   }
               });


           }
       }

        if(props.role === ROLE_ADMIN){
            getData<Vet[]>("/api/vets").then(vets => {
                if(vets) {
                    setVets(vets);
                }
            });
        }

        fetchAppointments();
        if(updateList){
            //fetchAppointments();
            setUpdateList(false)
        }

    }, [props.selectedVetId, props.role, props.userId, updateList]);

    let selectedVet = vets.find(vet => vet.id === props.selectedVetId);
    let title = (props.selectedVetId != null && selectedVet !== undefined) ?
        `${selectedVet.name}'s appointments` :
        "All appointments";

    let showAllBtn = <Button onClick={() => props.unselectVet()}>Show all appointments</Button>;
    let selectVetBtn = <VetDropdown vets={vets}/>;
    let topBtn = (props.selectedVetId != null) ? showAllBtn : selectVetBtn;

    const [showDetailsModal, setShowDetailsModal] = useState(false);
    const [aptDetails, setAptDetails] = useState<AppointmentDetails | null>(null);

    function handleAppointmentClick(args: any) {
        setShowDetailsModal(true);
        console.log(args.event);


        setAptDetails({
            id: args.event.extendedProps.appointmentId,
            start: Moment(args.event.start).format("YYYY-MM-DD HH:mm:ss").toString(),
            end: Moment(args.event.end).format("YYYY-MM-DD HH:mm:ss").toString(),
            vetId: args.event.extendedProps.vetId,
            clientId: args.event.extendedProps.clientId,
            petId: args.event.extendedProps.petId,
            isCompleted: args.event.extendedProps.isCompleted,
            description: args.event.extendedProps.description
        });
    }

    const [showNewAppointmentModal, setShowNewAppointmentModal] = useState(false);
    function handleNewAppointmentClick(){
        setShowNewAppointmentModal(true);
    }

    const handleCloseModalNewAppointment = () => {
        setShowNewAppointmentModal(false);
        setUpdateList(true);

    };
    const handleDetailsModalClose = () => {
        setShowDetailsModal(false);
        setUpdateList(true);
    };

    return (<>
        <Card>
            <Card.Header className='d-flex justify-content-between align-items-center'>
                {title}
                {props.role===ROLE_CLIENT && <Button onClick={handleNewAppointmentClick}>New Appointment</Button> }
            </Card.Header>
            <Card.Body>
                {(props.role === ROLE_ADMIN) && topBtn}
                <Calendar events={appointmentsList} handleEventClick={handleAppointmentClick}/>
            </Card.Body>
        </Card>
        {aptDetails &&
        <AppointmentDetailsModal showModal={showDetailsModal} handleClose={handleDetailsModalClose}
                                 details={aptDetails}/>
        }
        <CreateAppointmentForm clientId={props.userId} showModal={showNewAppointmentModal} handleClose={handleCloseModalNewAppointment}/>
    </>);
};


const mapStateToProps = (state: GlobalState) => ({
    selectedVetId: state.dashboard.appointmentsList.selectedVetId,
    userId: state.login.userId,
    role: state.login.role
});

const mapDispatchToProps =
    (dispatch: any) => ({
        unselectVet: () => {dispatch(unselectVet())}
    });

const AppointmentList = connect(mapStateToProps, mapDispatchToProps)(ProtoAppointmentList);
export default AppointmentList;