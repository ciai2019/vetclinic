import React from 'react';
import {connect} from "react-redux";

import {ListGroup} from 'react-bootstrap';
import {loadView} from "../actions";
import {VIEW_TYPES} from "../UserDashboard";
import {GlobalState} from "../../app/App";


const ProtoAdminPanelOptions = (
    props: {
        view: string,
        changeView: (view: string) => void
    }
) => {
    let defaultActiveKey = (props.view !== "") ? "#" + props.view : "#account";

    return(
        <ListGroup variant="flush" defaultActiveKey={defaultActiveKey}>
            <ListGroup.Item action href="#account"
                            onClick={() => {props.changeView(VIEW_TYPES.account)}}>
                Account
            </ListGroup.Item>
            <ListGroup.Item action href="#employees"
                            onClick={() => {props.changeView(VIEW_TYPES.employees)}}>
                Employees
            </ListGroup.Item>
            <ListGroup.Item action href="#clients"
                            onClick={() => {props.changeView(VIEW_TYPES.clients)}}>
                Clients
            </ListGroup.Item>
            <ListGroup.Item action href="#pets"
                            onClick={() => {props.changeView(VIEW_TYPES.pets)}}>
                Pets
            </ListGroup.Item>
            <ListGroup.Item action href="#appointments"
                            onClick={() => {props.changeView(VIEW_TYPES.appointments)}}>
                Appointments
            </ListGroup.Item>
        </ListGroup>
    );
};

const mapStateToProps = (state: GlobalState) => ({view: state.dashboard.view});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            changeView: (view: string) => { dispatch(loadView(view)) }
        });

const AdminPanelOptions = connect(mapStateToProps, mapDispatchToProps)(ProtoAdminPanelOptions);
export default AdminPanelOptions;