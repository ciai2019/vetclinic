import React, {useEffect, useState} from "react";
import {Container, Modal, Button} from 'react-bootstrap';
import ShiftForm from "./ShiftForm";
import Moment from 'moment';
import {CalendarEvent} from "../../Calendar";
import {getData, postData} from "../../../../network_utils";
import VetScheduleModal from "./VetScheduleModal";

interface TimeSlot {
    id: number,
    startDate: string,
    finishDate: string,
    vetId: number,
    appointmentId: number | null
}

interface NewTimeSlot {
    startDate: string,
    endDate: string
}

export interface PendingShift extends CalendarEvent{
    title: string,
    start: string,
    end: string,
    color: string,
}

const EditVetSchedule = (
    props: {
        title: string,
        vetId: number | null
    }
) => {
    const [schedule, setSchedule] = useState([] as CalendarEvent[]);
    const [pendingShifts, setPendingShifts] = useState([] as CalendarEvent[]);
    const [scheduleUpdated, setScheduleUpdated] = useState(true);

    useEffect(() => {
        if(scheduleUpdated){
            getData<TimeSlot[]>(`/api/vets/${props.vetId}/schedule`).then(schedule => {
                if(schedule) {
                    setSchedule(schedule.map(timeSlot => ({
                        title: "Shift",
                        start: timeSlot.startDate,
                        end: timeSlot.finishDate
                    })));
                }
            });

            setScheduleUpdated(false);
        }

    }, [props.vetId, scheduleUpdated]);


    const initialShiftFormState = {
        show: false,
        date: new Date()
    };
    const [shiftFormState, setShiftFormState] = useState(initialShiftFormState);

    function addShift(args: any){
        setShiftFormState({
           show: true,
           date: args.date
        });
    }

    function buildTimeSlots(firstDate: Date, lastDate: Date){
        let date = firstDate;
        let timeSlots = [] as NewTimeSlot[];

        while(date < lastDate){
            let startDateString = Moment(date).format("YYYY-MM-DDTHH:mm:ss").toString();

            let endDate = Moment(date).add(30, 'm');
            let endDateString = endDate.format("YYYY-MM-DDTHH:mm:ss").toString();

            timeSlots.push({
                startDate: startDateString,
                endDate: endDateString
            });

            date = endDate.toDate();
        }
        return timeSlots;
    }

    function submitSchedule() {
        let timeSlots = [] as NewTimeSlot[];

        // Create time slots for all shifts
        for(let i = 0; i < pendingShifts.length; i++){
            timeSlots = timeSlots.concat(buildTimeSlots(
               new Date(pendingShifts[i].start),
               new Date(pendingShifts[i].end)
            ));
        }

        let payload = {
            firstDate: pendingShifts[0].start,
            lastDate: pendingShifts[pendingShifts.length - 1].end,
            timeSlots: timeSlots
        };
        console.log(payload);

        postData(`/api/vets/${props.vetId}/schedule`, payload).then((response) => {
            if(response.status === 200) {
                alert("Schedule updated successfully!");
                setScheduleUpdated(true);
            }
        }).catch(error => {
            alert(error.response.data.message);
        });
    }

    function handleShiftFormSubmit(shift: any){
        if(shift){
            setPendingShifts(pendingShifts.concat(shift));
            setSchedule(schedule.concat(shift));
        }

        setShiftFormState(initialShiftFormState);
    }

    function handleScheduleSubmit(){
        submitSchedule();
    }

    return(
      <>
          <Modal.Header closeButton>
              <Modal.Title>
                  {props.title}
              </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
                <VetScheduleModal schedule={schedule} handleDateClick={addShift}/>
            </Container>
          </Modal.Body>
          <Button variant="primary" block onClick={handleScheduleSubmit}>Submit Changes</Button>
          {shiftFormState.show && <ShiftForm
              show={shiftFormState.show}
              date={shiftFormState.date}
              onSubmit={handleShiftFormSubmit}/>}
      </>
    );
};

export default EditVetSchedule;