import React, {FormEvent, useState} from "react";
import {Form, Modal, Button, FormControlProps} from "react-bootstrap";
import {PendingShift} from "./EditVetSchedule";
import Moment from 'moment';


interface ShiftHours {
    startHours?: string,
    startMinutes?: string,
    endHours?: string,
    endMinutes?: string
}

const ShiftForm = (
    props: {
        show: boolean,
        date: Date,
        onSubmit: (payload: any) => void
    }
) => {
    function prependZero(num: number){
        return ("0" + num).slice(-2);
    }

    const initialState: ShiftHours = {
        startHours: prependZero(props.date.getHours()),
        startMinutes: prependZero(props.date.getMinutes()),
        endHours: prependZero(Moment(props.date).add(30, 'm').toDate().getHours()),
        endMinutes: prependZero(Moment(props.date).add(30, 'm').toDate().getMinutes())
    };

    const [shiftHours, setShiftHours] = useState(initialState);

    function handleSubmit(e:FormEvent<HTMLFormElement>) {
        e.preventDefault();
        //console.log(shiftHours);
        let date = Moment(props.date).format("YYYY-MM-DD").toString();

        let shift: PendingShift = {
            title: "Shift",
            start: `${date}T${shiftHours.startHours}:${shiftHours.startMinutes}:00`,
            end: `${date}T${shiftHours.endHours}:${shiftHours.endMinutes}:00`,
            color: "#f7d111"
        };
        props.onSubmit(shift);
    }

    let handleStartHoursChange = (e: React.FormEvent<HTMLInputElement>) =>
        setShiftHours({...shiftHours, startHours: e.currentTarget.value as string});

    let handleStartMinutesChange = (e: React.FormEvent<HTMLInputElement>) =>
        setShiftHours({...shiftHours, startMinutes: e.currentTarget.value as string});

    let handleEndHoursChange = (e: React.FormEvent<HTMLInputElement>) =>
        setShiftHours({...shiftHours, endHours: e.currentTarget.value as string});

    let handleEndMinutesChange = (e: React.FormEvent<HTMLInputElement>) =>
        setShiftHours({...shiftHours, endMinutes: e.currentTarget.value as string});

    let startHourOptions = [];
    for(let i = 0; i <= 24; i++){
        // Prepend 0 to single digit numbers
        let value = prependZero(i);
        startHourOptions.push(<option key={value} value={value}>{value}</option>);
    }

    let minuteOptions = [];
    for(let i = 0; i < 60; i += 30){
        let value = prependZero(i);
        minuteOptions.push(<option key={value} value={value}>{value}</option>);
    }

    return(
        <Modal
            show={props.show}
            onHide={() => {props.onSubmit(null)}}
        >
            <Modal.Header closeButton>
                <Modal.Title>Add a new shift</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Row>
                        <Form.Group controlId="shiftForm.StartHours">
                            <Form.Label>Starts at:</Form.Label>
                            <Form.Control as="select" defaultValue={initialState.startHours} onChange={handleStartHoursChange}>
                                {startHourOptions}
                            </Form.Control>
                            <Form.Control as="select" defaultValue={initialState.startMinutes}  onChange={handleStartMinutesChange}>
                                {minuteOptions}
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group controlId="shiftForm.EndHours">
                            <Form.Label>Ends at:</Form.Label>
                            <Form.Control as="select" defaultValue={initialState.endHours} onChange={handleEndHoursChange}>
                                {startHourOptions}
                            </Form.Control>
                            <Form.Control as="select" defaultValue={initialState.endMinutes} onChange={handleEndMinutesChange}>
                                {minuteOptions}
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>
                    <Button type="submit">Add</Button>
                </Form>
            </Modal.Body>
        </Modal>
    );

};

export default ShiftForm;