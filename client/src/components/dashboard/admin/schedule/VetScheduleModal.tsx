import React from "react";
import Calendar, {CalendarEvent} from "../../Calendar";

const VetScheduleModal = (
    props: {
        schedule: CalendarEvent[],
        handleDateClick?: (arg: any) => void,
    }
) => {
    return <Calendar events={props.schedule} handleDateClick={props.handleDateClick}/>
};

export default VetScheduleModal;