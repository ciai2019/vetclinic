import React, {useState} from "react";
import {connect} from "react-redux";
import {selectVet} from "../../actions";
import {Dropdown, Button, FormControl, Image} from "react-bootstrap";

import '../styles/EmployeeList.css'
import {Vet} from "../employees/VetDetailsModal";


const VetDropdownToggle = React.forwardRef<any, any>(({ children, onClick }, ref) => (
    <Button
        variant="primary"
        ref={ref}
        onClick={(e: any) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        {children}
        &#x25bc;
    </Button>
));

const VetDropdownMenu = React.forwardRef<any, any>(({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
    const [value, setValue] = useState("");

    return (
        <div
            ref={ref}
            style={style}
            className={className}
            aria-labelledby={labeledBy}
        >
            <FormControl
                autoFocus
                className="mx-3 my-2 w-auto"
                placeholder="Type to filter..."
                onChange={(e: any) => setValue(e.target.value)}
                value={value}
            />
            <ul className="list-unstyled">
                {React.Children.toArray(children).filter(
                    child =>
                        !value || child.props.children[1].toLowerCase().startsWith(value),
                )}
            </ul>
        </div>
    );
});

const ProtoVetDropdown = (
    props: {
        vets: Vet[],
        selectVet: (vetId: number) => void
    }
) => {

    let dropdownItems = props.vets.map((vet, index) => (
        <Dropdown.Item key={index}  onClick={()=> props.selectVet(vet.id)}>
            <div className="employee-img-container d-inline-block">
                <Image src={vet.picture} alt="Vet avatar" fluid roundedCircle/>
            </div>
            {vet.name}
        </Dropdown.Item>
    ));

    return (
        <Dropdown>
            <Dropdown.Toggle as={VetDropdownToggle} id="dropdown-custom-components">
                Select vet
            </Dropdown.Toggle>
            <Dropdown.Menu as={VetDropdownMenu}>
                {dropdownItems}
            </Dropdown.Menu>
        </Dropdown>
    );
};

const mapDispatchToProps =
    (dispatch: any) => ({
       selectVet: (vetId: number) => {dispatch(selectVet(vetId))}
    });

const VetDropdown = connect(null, mapDispatchToProps)(ProtoVetDropdown);
export default VetDropdown;