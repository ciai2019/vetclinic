import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Card, ListGroup, Nav, Tab, Form, FormControlProps} from "react-bootstrap";
import {GlobalState} from "../../../app/App";
import VetDetailsModal from "./VetDetailsModal";
import '../styles/EmployeeList.css'
import {ROLE_ADMIN} from "../../UserDashboard";
import VetList from "./VetList";
import AdminList from "./AdminList";
import {getData} from "../../../../network_utils";

export interface Employee {
    id: number,
    name: string,
    picture: string,
    email: string,
    phoneNumber: string,
    role: string
}

const ProtoEmployeeList = (
    props: {
        isLoggedIn: boolean,
        role: string
    }
) => {
    const [employeeList, setEmployeeList] = useState([] as Employee[]);
    const [filter, setFilter] = useState("");

    let handleFilterChange = (e: React.FormEvent<HTMLInputElement>) => setFilter(e.currentTarget.value as string);

    let filterInput = <Form.Control type="text" placeholder="Filter by name"
                                    onChange={(e: React.FormEvent<HTMLInputElement>) => {handleFilterChange(e)}}/>;

    useEffect(() => {
        getData<Employee[]>("/api/employees", {
            name: filter
        }).then(employees => {
            if(employees){
                setEmployeeList(employees);
            }
        });

    }, [filter]);

    let admins = employeeList.filter(emp => emp.role === 'ROLE_ADMIN');
    let vets = employeeList.filter(emp => emp.role === 'ROLE_VET');


    const [showModal, setShowModal] = useState(false);
    const handleCloseModal = () => setShowModal(false);

    const [selectedVetId, setSelectedVetId] = useState<number | null>(null);

    function canViewDetails(){
        return props.isLoggedIn && props.role === ROLE_ADMIN
    }

    function handleVetClick(vetId: number){
        if(canViewDetails()){
            setSelectedVetId(vetId);
            setShowModal(true);

        }
    }

    return (<>
        <Tab.Container defaultActiveKey="vets">
            <Card>
                <Card.Header>
                    <Nav variant="tabs">
                        <Nav.Item>
                            <Nav.Link eventKey="vets">Veterinarians</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="admins">Administrators</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Card.Header>
                <Card.Body>
                    <Tab.Content>
                        {props.isLoggedIn && filterInput}
                        <Tab.Pane eventKey="vets">
                            <ListGroup variant="flush">
                                <VetList vets={vets} handleVetClick={handleVetClick} canViewDetails={canViewDetails}/>
                            </ListGroup>
                        </Tab.Pane>
                        <Tab.Pane eventKey="admins">
                            <ListGroup variant="flush">
                                <AdminList admins={admins} canViewDetails={canViewDetails}/>
                            </ListGroup>
                        </Tab.Pane>
                    </Tab.Content>
                </Card.Body>
            </Card>
        </Tab.Container>
        <VetDetailsModal handleClose={handleCloseModal} showModal={showModal} vetId={selectedVetId}/>
        </>
    );
};

const mapStateToProps = (state:GlobalState) => ({isLoggedIn: state.login.isLoggedIn, role: state.login.role});

const EmployeeList = connect(mapStateToProps)(ProtoEmployeeList);
export default EmployeeList;