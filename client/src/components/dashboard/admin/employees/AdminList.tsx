import React from "react";
import {Employee} from "./EmployeeList";
import {Image, ListGroup} from "react-bootstrap";
import admin_avatar from "../../img/default_vet_avatar.png";

const AdminList = (
    props: {
        admins: Employee[],
        canViewDetails: () => boolean
    }
) => {

    let adminsList = props.admins.map(admin =>
        <ListGroup.Item action={props.canViewDetails()} key={ admin.id } >
            <div className="employee-img-container d-inline-block">
                <Image src={(admin.picture.length !== 0) ? admin.picture : admin_avatar} fluid roundedCircle/>
            </div>
            {admin.name}
        </ListGroup.Item>
    );

    return (
        <ListGroup variant="flush">
            {adminsList}
        </ListGroup>
    );
};

export default AdminList;