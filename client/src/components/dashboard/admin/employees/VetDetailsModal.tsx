import React, {useEffect, useState} from "react";
import {Col, Modal, Button} from "react-bootstrap";
import EditVetSchedule from "../schedule/EditVetSchedule";
import UserDetailsModal, {User} from "../../UserDetailsModal";
import {selectVetAppointments} from "../../actions";
import {connect} from "react-redux";
import {getData} from "../../../../network_utils";

export interface Vet extends User{
    id: number,
    name: string,
    picture: string,
    email: string,
    phoneNumber: string,
    address: string,
    username: string,
    frozenAccount: boolean
}

const ProtoVetDetailsModal = (
    props: {
        vetId: number | null,
        showModal: boolean,
        handleClose: () => void,
        selectVetAppointments: (vetId: number) => void
    }
) => {

    const initialState: Vet = {
        id: -1,
        name: "",
        picture: "",
        email: "",
        phoneNumber: "",
        address: "",
        username: "",
        frozenAccount: false
    };
    const [vetDetails, setVetDetails] = useState<Vet>(initialState);

    useEffect(() => {
        if(props.showModal && props.vetId){
            //fetchVetDetails();
            getData<Vet>(`/api/vets/${props.vetId}`).then(vet => {
                if(vet){
                    setVetDetails(vet);
                }
            })
        }
    }, [props.vetId, props.showModal]);


    function changeModalView(modalView: any){
        setModalViewType(modalView);
    }

    function handleVetAppointmentsClick() {
        if(props.vetId){
            props.selectVetAppointments(props.vetId);
        }
    }

    const VET_DETAILS_VIEW = "VET_DETAILS_VIEW";
    const VET_SCHEDULE_VIEW = "VET_SCHEDULE_VIEW";
    const VET_APPOINTMENTS_VIEW = "VET_APPOINTMENTS_VIEW";

    const [modalViewType, setModalViewType] = useState(VET_DETAILS_VIEW);

    let vetScheduleModalView = <EditVetSchedule title={`${vetDetails.name}'s schedule`} vetId={props.vetId}/>;

    let vetDetailsModalView = (<>
        <UserDetailsModal title={"Vet Details"} user={vetDetails}>
            <Col>
                <Button variant="primary" size="lg" block onClick={() => changeModalView(VET_SCHEDULE_VIEW)}>Schedule</Button>
                <Button variant="primary" size="lg" block onClick={handleVetAppointmentsClick}>Appointments</Button>
            </Col>
        </UserDetailsModal>
    </>);

    let modalView;

    switch (modalViewType) {
        case VET_DETAILS_VIEW:
            modalView = vetDetailsModalView;
            break;
        case VET_SCHEDULE_VIEW:
            modalView = vetScheduleModalView;
            break;
        case VET_APPOINTMENTS_VIEW:
            modalView = vetDetailsModalView;
            break;
    }

    return(
        <Modal
            show={props.showModal}
            onHide={() => {props.handleClose(); setModalViewType(VET_DETAILS_VIEW); }}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            {modalView}
        </Modal>
    );
};

const mapDispatchToProps =
    (dispatch: any) => ({
       selectVetAppointments: (vetId: number) => {dispatch(selectVetAppointments(vetId))}
    });

const VetDetailsModal = connect(null, mapDispatchToProps)(ProtoVetDetailsModal);
export default VetDetailsModal;