import React from "react";
import {Employee} from "./EmployeeList";
import {Image, ListGroup} from "react-bootstrap";
import vet_avatar from "../../img/default_vet_avatar.png";

const VetList = (
    props: {
        vets: Employee[],
        handleVetClick: (vetId: number) => void
        canViewDetails: () => boolean
    }
) => {

    let vetsList = props.vets.map(vet =>
        <ListGroup.Item action={props.canViewDetails()} key={ vet.id } onClick={() => { props.handleVetClick(vet.id) }}>
            <div className="employee-img-container d-inline-block">
                <Image src={(vet.picture.length !== 0) ? vet.picture : vet_avatar} fluid roundedCircle/>
            </div>
            {vet.name}
        </ListGroup.Item>
    );

    return (
        <ListGroup variant="flush">
            {vetsList}
        </ListGroup>
    );
};

export default VetList;