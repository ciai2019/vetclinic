import React from 'react';
import {connect, Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunk from 'redux-thunk'
import {createLogger} from "redux-logger";

import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Home from '../homepage/Home';
import LoginForm, {LoginState} from "../login/LoginForm";
import './App.css';
import reducer from "./reducers";
import UserDashboard, {DashboardState} from "../dashboard/UserDashboard";

import http from '../../http';

export interface GlobalState {
    login: LoginState,
    dashboard: DashboardState
}

const ProtoPage = (props: { isLoggedIn: boolean }) => {
    return (
        <Router>
            <Switch>
                <Route path="/dashboard">
                    {props.isLoggedIn ? <UserDashboard/> : <Redirect to={"/login"}/>}
                </Route>
                <Route path="/login">
                    {props.isLoggedIn ? <Redirect to={"/dashboard"}/> :  <LoginForm/>}
                </Route>
                <Route path="/">
                    <Home/>
                </Route>
            </Switch>
        </Router>
    );
};

const mapStateToProps = (state: GlobalState) => ({isLoggedIn: state.login.isLoggedIn});
const Page = connect(mapStateToProps)(ProtoPage);

const logger = createLogger();
const store = createStore(reducer, applyMiddleware(thunk, logger));

http.setup(store);

const App = () => {
  return (
      <Provider store={store}>
          <Page/>
      </Provider>
  );
};

export default App;
