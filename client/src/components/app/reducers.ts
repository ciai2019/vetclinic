import {combineReducers} from "redux";
import loginReducer from "../login/reducers";
import dashboardReducer from "../dashboard/reducers";

const reducer = combineReducers({
    login: loginReducer,
    dashboard: dashboardReducer
});

export default reducer;