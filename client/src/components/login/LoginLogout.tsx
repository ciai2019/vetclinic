import React, {FormEvent} from 'react';
import {connect} from "react-redux";

import {GlobalState} from '../app/App';
import {logOut} from "./actions";

import {Button} from 'react-bootstrap';
import './styles/LoginLogout.css';

const ProtoLoginLogout = (
    props: {
        isLoggedIn: boolean,
        performLogout: () => void
    }
) => {
    let logoutHandler = (e: FormEvent<HTMLButtonElement>) => {props.performLogout()};

    let loginButton = <Button variant="link" className="login" href="/login">Login</Button>;
    let logoutButton = <Button className="logout" onClick={logoutHandler}>Logout</Button>;

    return (
        <div className="login-logout-container">
            {props.isLoggedIn ? logoutButton : loginButton}
        </div>
    );
};

const mapStateToProps = (state: GlobalState) => ({isLoggedIn: state.login.isLoggedIn});
const mapDispatchToProps = (dispatch: any) => ({
   performLogout: () => dispatch(logOut())
});

const LoginLogout = connect(mapStateToProps, mapDispatchToProps)(ProtoLoginLogout);

export default LoginLogout;