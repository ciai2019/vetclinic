import {Action} from "redux";
//import http from '../../http';
import axios from 'axios';

export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';

export interface LoginAction extends Action{
    //token: string | null,
    userId: string | null,
    role: string | null
}

export const logIn = (userId: string | null, role: string | null) => ({
    type: LOG_IN,
    //token: token,
    userId: userId,
    role: role
});

export const logOut = () => ({type: LOG_OUT});

export function requestLogin(username: string, password: string) {
    return (dispatch: any) =>
        performLogin(username, password)
            .then(data => {
                if(data){
                    dispatch(logIn(data.userId, data.role))
                }
            })
}

function parseJwt(token: string){
    return JSON.parse(atob(token.split('.')[1]));
}

async function performLogin(username: string, password: string) {
    return axios.post("/login", {
        username: username,
        password: password
    }).then((response) => {
        if(response.status === 200){
            let token = response.headers["authorization"];

            if(token) {
                let data = parseJwt(token);
                console.log(data);

                return axios.get(`api/users/${data["username"]}/id`).then((response) => {
                    if(response.status === 200){
                        return {
                            userId: response.data["userId"],
                            role: data["role"]
                        }
                    }
                })

            }
        } else {
            alert("Authentication error please retry")
            console.log(`Error: ${response.status}: ${response.statusText}`);
            return null;
            // and add a message to the Ui: wrong password ?? other errors?
        }
    }).catch((error) => {
        alert("Authentication error please retry")
        console.log(error);
        return null;
    })

    /*
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return fetch("/login",
        {method:'POST',
            headers: headers,
            body: JSON.stringify({username:username, password:password})})
        .then( response => {
            if( response.ok ) {
                let token = response.headers.get("Authorization");
                if (token) {
                    let data = parseJwt(token);
                    console.log(data);

                    return {
                        token: token,
                        username: data["username"],
                        role: data["role"]
                    }
                }


            }else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch( err => { console.log(err); return null })

     */

}