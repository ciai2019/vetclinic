import {Action} from "redux";
import {LOG_IN, LOG_OUT, LoginAction} from "./actions";

function checkIfTokenIsValid() {
    return localStorage.getItem('jwt') != null;
}

function getUserId() {
    let userId = localStorage.getItem('userId');

    if(userId == null){
        return null;
    } else {
        return userId;
    }
}

function getRole() {
    let role = localStorage.getItem('role');

    if(role == null){
        return "";
    } else {
        return role;
    }
}

const initialState = {
    isLoggedIn: checkIfTokenIsValid(),
    userId: getUserId(),
    role: getRole()
};

function loginReducer(state = initialState, action: Action) {
    switch (action.type) {
        case LOG_IN:
            //let token = (action as LoginAction).token;
            let userId = (action as LoginAction).userId;
            let role = (action as LoginAction).role;

            if(/*token && */ userId && role){
                //localStorage.setItem('jwt', token);
                localStorage.setItem('userId', userId);
                localStorage.setItem('role', role);

                return {...state, isLoggedIn: true, userId: userId, role: role};
            }

            break;

        case LOG_OUT:
            localStorage.removeItem('jwt');
            localStorage.removeItem('userId');
            localStorage.removeItem('role');

            return {...state, isLoggedIn: false, userId: null, role: ""};

        default:
            return state;
    }
}

export default loginReducer;