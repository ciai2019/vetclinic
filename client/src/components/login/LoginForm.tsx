import React, {ChangeEvent, FormEvent, useState} from "react";
import {Redirect} from 'react-router-dom';

import {connect} from "react-redux";

import {GlobalState} from '../app/App';
import {requestLogin} from "./actions";

export interface LoginState {
    isLoggedIn: boolean,
    userId: string,
    role: string
}

const ProtoLoginForm = (
    props: {
        isLoggedIn: boolean,
        performLogin: (username: string, password: string) => void
    }
) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let onSubmitHandler = (e:FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        props.performLogin(username, password);
    };

    let usernameChangedHandler = (e: ChangeEvent<HTMLInputElement>) => { setUsername(e.target.value) };
    let passwordChangedHandler = (e: ChangeEvent<HTMLInputElement>) => { setPassword(e.target.value) };

    let loginForm =
        <form onSubmit={onSubmitHandler}>
            <div>
                <label>Username: <input type="text" value={username} onChange={usernameChangedHandler}/></label>
            </div>
            <div>
                <label>Password: <input type="password" value={password} onChange={passwordChangedHandler}/></label>
            </div>
            <button>Login</button>
        </form>;

    let redirect = <Redirect to={"/"}/>;

    return (<> {props.isLoggedIn ? redirect : loginForm} </>);
};

const mapStateToProps = (state: GlobalState) => ({isLoggedIn: state.login.isLoggedIn});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            performLogin: (username: string, password: string) => {dispatch(requestLogin(username, password))}
        });

const LoginForm = connect(mapStateToProps, mapDispatchToProps)(ProtoLoginForm);

export default LoginForm;