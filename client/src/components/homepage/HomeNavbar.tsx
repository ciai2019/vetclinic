import React from 'react';
import {Navbar} from 'react-bootstrap';
import logo from './img/logo.svg';
import LoginLogout from "../login/LoginLogout";

import './styles/HomeNavbar.css';

const HomeNavbar:React.FC = () => {
    return (
      <Navbar bg="light" className="home-navbar justify-content-between gradient-bg" expand="lg">
          <Navbar.Brand>
              <img
                  alt=""
                  src={logo}
                  width="40"
                  height="40"
                  className="d-inline-block"
              />
              Vet Clinic
          </Navbar.Brand>

          <LoginLogout/>
      </Navbar>
    );
};

export default HomeNavbar;