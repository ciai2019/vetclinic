import React from 'react';
import {Nav} from 'react-bootstrap';
import './styles/HomeNav.css';

const HomeNav:React.FC = () => (
    <Nav className="home-nav" variant="pills" defaultActiveKey="home">
        <Nav.Item>
            <Nav.Link href="#home" eventKey="home">Home</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link href="#staff" eventKey="staff">Staff</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link href="#contacts" eventKey="contacts">Contacts</Nav.Link>
        </Nav.Item>

    </Nav>
);

export default HomeNav;