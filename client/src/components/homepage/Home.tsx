import React from 'react';
import {Row, Col} from 'react-bootstrap';
import './styles/Home.css';
import HomeNavbar from './HomeNavbar';
import HomeCarousel from "./HomeCarousel";
import EmployeeList from "../dashboard/admin/employees/EmployeeList";

const Home:React.FC = () => {

  const homePage = (
      <div className="home-container">
        <HomeNavbar/>
        <HomeCarousel/>
        <Row className="employees-container">
          <Col xs={12}>
            <h2>Meet our staff</h2>
          </Col>
          <Col xs={12}>
            <Row className="justify-content-center">
              <Col xs={12}>
                <EmployeeList/>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
  );

  return (
      homePage
  );
};

export default Home;
