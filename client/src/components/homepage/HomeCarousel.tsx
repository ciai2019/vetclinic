import React from 'react';
import {Carousel, CarouselItem} from 'react-bootstrap';
import photo1 from './img/family.jpg';
import photo2 from './img/vet.jpg';
import photo3 from './img/cool_monkey.jpg';

import './styles/HomeCarousel.css'

const HomeCarousel:React.FC = () => (
    <Carousel className="home-carousel" controls={false} pauseOnHover={false} fade={true}>
        <CarouselItem>
            <img
                className="d-block w-100"
                src={photo1}
                alt="First Slide"
            />
        </CarouselItem>
        <CarouselItem>
            <img
                className="d-block w-100"
                src={photo2}
                alt="Second Slide"
            />
        </CarouselItem>
        <CarouselItem>
            <img
                className="d-block w-100"
                src={photo3}
                alt="Third Slide"
            />
        </CarouselItem>
    </Carousel>
);

export default HomeCarousel;