import axios from 'axios';
import {LOG_OUT} from "../components/login/actions";

//const axiosInstance = axios.create();

export default {

    setup: (store: any) => {

        // Config for every request
        axios.interceptors.request.use((config) => {
            let token = localStorage.getItem("jwt");

            if(token){
                config.headers["Authorization"] = token;
            }

            config.headers["Content-Type"] = "application/json";
            return config;
        }, (error) => {
            return Promise.reject(error);
        });

        // Config for every response
        axios.interceptors.response.use((response) => {
            let newToken = response.headers["authorization"];

            if(newToken){
                localStorage.setItem("jwt", newToken);
            }

            return response;
        }, (error) => {
            if(error.response.status === 401) {
                console.log("Token expired, redirecting to login page...");
                store.dispatch({type: LOG_OUT});
            }

            return Promise.reject(error);
        });
    }

}
