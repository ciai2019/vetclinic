import axios from 'axios';

export async function getData<T>(url: string, params?: any) {
    return axios.get<T>(url, {
            params: params
        }).then((response) => {
            if(response.status === 200) {
                return response.data;
            }
        }).catch((error) => {
            console.log(error.response.data);
            return null;
        });
}

export async function postData(url: string, payload: any) {
    return axios.post(url, payload);
}