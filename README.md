# VetClinic
Project repo for CIAI course 2019

# REST COMMANDS

(POST) curl -i -H "Content-Type: application/json" -X POST -d '{"name": "zeca", "species": "gato" , "age" : "1", "physicalDescription" : "good" , "notes": "Is a bad boy"}' http://localhost:8080/api/pets

(GET all) curl -i -H 'Accept: application/json' http://localhost:8080/api/pets

(GET 1) curl -i -H 'Accept: application/json' http://localhost:8080/api/pets/1

(UPDATE) curl -i -H "Content-Type: application/json" -X PUT -d '{"name": "zeca", "species": "gato" , "age" : "1", "physicalDescription" : "good" , "notes": "Is a bad boy"}' http://localhost:8080/api/pets/1

(DELETE) curl -i -X DELETE http://localhost:8080/api/pets/1

# SETUP MYSQL

## install mysql
sudo apt-get update
sudo apt-get install mysql-server
sudo systemctl start mysql

## login as root
sudo mysql -u root -p

## create user && give all permissions
CREATE USER 'springuser'@'localhost' IDENTIFIED BY 'ThePassword';
GRANT ALL PRIVILEGES ON * . * TO 'springuser'@'localhost';
FLUSH PRIVILEGES;

## login as springuser
mysql -u springuser -p
 
## create and use database
CREATE DATABASE vetclinic;
USE vetclinic;

//Não é preciso criar tabela, o spring já faz isso automáticamente


