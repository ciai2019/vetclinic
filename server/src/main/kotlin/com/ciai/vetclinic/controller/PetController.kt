package com.ciai.vetclinic.controller


import com.ciai.vetclinic.controller.exceptions.handleExceptions
import com.ciai.vetclinic.model.pet.NewPetDTO
import com.ciai.vetclinic.model.pet.PetDTO
import com.ciai.vetclinic.model.pet.UpdatePetDTO
import com.ciai.vetclinic.security.*
import com.ciai.vetclinic.service.PetService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid


@Api(value = "VetClinic Management System - Pet API",
        description = "Management operations for Pets in the IADI 2019 PET Clinic")

@RestController
@RequestMapping("/api")
@CrossOrigin
class PetController(private val pets: PetService) {

    @ApiOperation(value = "View a list of registered pets", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved list"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource")
    ])
    @GetMapping("/pets")
    @IsAdminOrVet
    fun getAllPets(@RequestParam(required = false) name: Optional<String>) =
            pets.getAllPets(name)


    @ApiOperation(value = "Add a new pet", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added a pet"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PostMapping("/pets")
    //@IsAllowedForAddNewPet
    fun addNewPet(@Valid @RequestBody newPetDTO: NewPetDTO): PetDTO =
            pets.addNewPet(newPetDTO)


    @ApiOperation(value = "Get the details of a single pet by id", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved pet details"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        ApiResponse(code = 404, message = "The pet you were trying to reach is not found")
    ])
    @GetMapping("/pets/{id}")
    @IsAllowedPetOwnerOrAdminOrVet
    fun getPetById(@PathVariable(value = "id") petId: Long): PetDTO =
            handleExceptions {
                pets.getOnePet(petId)
            }


    @ApiOperation(value = "Update a pet", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated a pet"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PutMapping("/pets/{id}")
    @IsAllowedPetOwner
    fun updatePetById(@PathVariable(value = "id") petId: Long,
                      @Valid @RequestBody newPet: UpdatePetDTO): PetDTO =
            handleExceptions {
                pets.updatePet(petId, newPet)
            }


    @ApiOperation(value = "Delete a pet", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted a pet"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @DeleteMapping("/pets/{id}")
    @IsAllowedPetOwner
    fun deletePetById(@PathVariable(value = "id") petId: Long) =
            handleExceptions {
                pets.deletePet(petId)
            }
}