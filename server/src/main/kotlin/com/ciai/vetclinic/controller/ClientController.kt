package com.ciai.vetclinic.controller

import com.ciai.vetclinic.controller.exceptions.handleExceptions
import com.ciai.vetclinic.model.appointment.AppointmentDTO
import com.ciai.vetclinic.model.user.client.Client
import com.ciai.vetclinic.model.user.client.ClientDTO
import com.ciai.vetclinic.model.user.client.NewClientDTO
import com.ciai.vetclinic.model.user.client.UpdateClientDTO
import com.ciai.vetclinic.model.pet.PetDTO
import com.ciai.vetclinic.security.*
import com.ciai.vetclinic.service.ClientService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@Api(value = "VetClinic Management System - Client API",
        description = "Management operations for Clients in the IADI 2019 PET Clinic")

@RestController    // This means that this class is a Controller
@RequestMapping("/api") // This means URL's start with /demo (after Application path)
class ClientController(val clients: ClientService) {

    @ApiOperation(value = "View a list of all clients", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all client details"),
        ApiResponse(code = 404, message = "The clients does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/clients")
    @IsAdmin
    fun getAllClients(): List<ClientDTO> =
            clients.getAllClients()

    @ApiOperation(value = "Get the information of a client by id", response = ClientDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved client details"),
        ApiResponse(code = 404, message = "The client with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/clients/{id}")
    @IsAllowedClientOrAdminOrVet
    fun getClientById(@PathVariable(value = "id") clientId: Long): ClientDTO =
            handleExceptions {
                clients.getClient(clientId)
            }


    @ApiOperation(value = "Create a new client", response = ClientDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added client"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PostMapping("/clients")
    fun createNewClient(@RequestBody client: NewClientDTO): ClientDTO =
            clients.createNewClient(client)

    @ApiOperation(value = "View the list of appointments scheduled for a client", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved appointment details"),
        ApiResponse(code = 404, message = "The appointment with the supplied client id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/clients/{id}/appointments")
    @IsAllowedClientOrAdmin
    fun getClientAppointmentsById(@PathVariable(value = "id") clientId: Long): List<AppointmentDTO> =
            handleExceptions {
                clients.getClientAppointmentsById(clientId)
            }


    @ApiOperation(value = "View the list of pets owned by a client", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved client details"),
        ApiResponse(code = 404, message = "The pets with the supplied clients id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    /*
    @GetMapping("/clients/{id}/pets")
    @IsAllowedClientOrAdmin
    fun getClientPetsById(@PathVariable(value = "id") clientId: Long): List<PetDTO> =
            handleExceptions {
                clients.getClientPetsById(clientId)
            }

    */
    @GetMapping("/clients/{id}/pets")
    @IsAllowedClientOrAdminOrVet
    fun getClientPetsByUsername(@PathVariable(value = "id") clientId: Long ,@RequestParam(required = false) name: Optional<String>): List<PetDTO> =
            handleExceptions {
                clients.getClientPetsByUsername(clientId,name)
            }
    /*
    @ApiOperation(value = "Update client information", response = Client::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated client information"),
        ApiResponse(code = 404, message = "The client with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PutMapping("/clients/{id}")
    fun updateClientById(@PathVariable(value = "id") clientId: Long,
                         @Valid @RequestBody newClientInfo: UpdateClientDTO): ClientDTO =
            handleExceptions {
                clients.updateClientById(clientId, newClientInfo)
            }

     */

    @ApiOperation(value = "Delete client")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted client"),
        ApiResponse(code = 404, message = "The client with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @DeleteMapping("/clients/{id}")
    @IsAllowedClient
    fun deleteClientById(@PathVariable(value = "id") clientId: Long) =
            handleExceptions {
                clients.deleteClient(clientId)
            }
}

