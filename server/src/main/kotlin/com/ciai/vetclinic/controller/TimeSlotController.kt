package com.ciai.vetclinic.controller

import com.ciai.vetclinic.model.timeslot.AvailableTimeSlotDTO
import com.ciai.vetclinic.model.timeslot.TimeSlotDTO
import com.ciai.vetclinic.security.IsAdminOrClient
import com.ciai.vetclinic.service.TimeSlotService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class TimeSlotController(private val service: TimeSlotService) {

    @GetMapping("timeslots/available")
    @IsAdminOrClient
    fun getAllAvailableSlots(): List<AvailableTimeSlotDTO> =
            service.getAllAvailableSlots()

    @GetMapping("timeslots/occupied")
    @IsAdminOrClient
    fun getAllOccupiedSlots(): List<TimeSlotDTO> =
            service.getAllOccupiedSlots()
}