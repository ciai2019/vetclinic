package com.ciai.vetclinic.controller

import com.ciai.vetclinic.controller.exceptions.handleExceptions
import com.ciai.vetclinic.model.user.UpdateUserDTO
import com.ciai.vetclinic.model.user.EmployeeDTO
import com.ciai.vetclinic.model.user.UserDTO
import com.ciai.vetclinic.model.user.UserIdDTO
import com.ciai.vetclinic.model.user.client.ClientDTO
import com.ciai.vetclinic.security.IsAllowedClientOrAdmin
import com.ciai.vetclinic.security.IsAllowedUser
import com.ciai.vetclinic.security.IsAllowedUserByUsername
import com.ciai.vetclinic.service.UsersService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
@CrossOrigin
class UserController(val users: UsersService) {


    @ApiOperation(value = "View a list of all employees", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all employees"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/employees")
    fun getAllEmployees(@RequestParam(required = false) name: Optional<String>): List<EmployeeDTO> =
            users.getAllEmployees(name)

    /*
    @ApiOperation(value = "Fetch information on one user", response = UserDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved user information"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])

    @GetMapping("/users/{username}")
    fun getUserInfo(@PathVariable(value = "username") username: String) =
            handleExceptions {
                users.getUserInfo(username)
            }
    */
    @GetMapping("/users/{id}")
    fun getUserInfo(@PathVariable(value = "id") userId: Long) =
            handleExceptions {
                users.getUserById(userId)
            }
    @GetMapping("/users/{username}/id")
    @IsAllowedUserByUsername
    fun getUserIdByUsername(@PathVariable(value = "username") username: String): UserIdDTO =
            handleExceptions {
                users.getUserIdByUsername(username)
            }


    @ApiOperation(value = "Update user information", response = UpdateUserDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated user information"),
        ApiResponse(code = 404, message = "The vet with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PutMapping("/users/{id}")
    @IsAllowedUser
    fun editUserInfo(@PathVariable(value = "id") userId: Long,
                     @Valid @RequestBody updateUserDTO: UpdateUserDTO): UpdateUserDTO =
            handleExceptions {
                users.editUserInfo(userId, updateUserDTO)
            }
}