package com.ciai.vetclinic.controller.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class HTTPNotFoundException(s: String) : Exception(s)

@ResponseStatus(HttpStatus.BAD_REQUEST)
class HTTPBadRequestException(s: String) : Exception(s)

@ResponseStatus(HttpStatus.CONFLICT)
class HTTPConflictException(s: String) : Exception(s)

@ResponseStatus(HttpStatus.ACCEPTED)
class HTTPAcceptedException(s: String) : Exception(s)

@ResponseStatus(HttpStatus.FORBIDDEN)
class HTTPForbiddenException(s: String) : Exception(s)