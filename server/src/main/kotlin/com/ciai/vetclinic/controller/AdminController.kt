package com.ciai.vetclinic.controller

import com.ciai.vetclinic.controller.exceptions.handleExceptions
import com.ciai.vetclinic.model.user.admin.AdminDTO
import com.ciai.vetclinic.model.user.admin.NewAdminDTO
import com.ciai.vetclinic.security.IsAdmin
import com.ciai.vetclinic.service.AdminService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(value = "VetClinic Management System - Admin API",
        description = "Management operations for Admins in the IADI 2019 PET Clinic")

@RestController
@RequestMapping("/api")
class AdminController (val adminService : AdminService){

    /*
    @EventListener(ApplicationReadyEvent::class)
    fun createDefaultAdmin() =
        adminService.createDefaultAdminIfNotPresent()

     */

    @ApiOperation(value = "Hire a new admin")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully hired a new admin "),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PostMapping("/admins")
    @IsAdmin
    fun hireNewAdmin(@Valid @RequestBody newAdmin: NewAdminDTO): AdminDTO =
            adminService.hireNewAdmin(newAdmin)

    /*
    @ApiOperation(value = "Update admin information", response = AdminDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated admin information"),
        ApiResponse(code = 404, message = "The admin with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PutMapping("/admins/{id}")
    fun updateAdminById(@PathVariable(value = "id") employeeId: Long,
                      @Valid @RequestBody newAdmin: UpdateAdminDTO): AdminDTO =
            handleExceptions {
                adminService.updateAdminById(employeeId, newAdmin)
            }

     */


    @ApiOperation(value = "Fire admin", response = AdminDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully fired admin"),
        ApiResponse(code = 404, message = "The admin with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @DeleteMapping("/admins/{id}")
    @IsAdmin
    fun fireAdmin(@PathVariable(value = "id") employeeId: Long) =
            handleExceptions {
                adminService.fireAdmin(employeeId)
            }

}