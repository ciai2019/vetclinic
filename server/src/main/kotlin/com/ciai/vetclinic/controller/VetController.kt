package com.ciai.vetclinic.controller

import com.ciai.vetclinic.controller.exceptions.handleExceptions
import com.ciai.vetclinic.model.appointment.AppointmentDTO
import com.ciai.vetclinic.model.appointment.CompleteAppointmentDTO
import com.ciai.vetclinic.model.timeslot.EditTimeSlotDTO
import com.ciai.vetclinic.model.timeslot.NewScheduleDTO
import com.ciai.vetclinic.model.timeslot.TimeSlotDTO
import com.ciai.vetclinic.model.user.vet.NewVetDTO
import com.ciai.vetclinic.model.user.vet.VetDTO
import com.ciai.vetclinic.security.IsAdmin
import com.ciai.vetclinic.security.IsAllowedVet
import com.ciai.vetclinic.security.IsAllowedVetOrAdmin
import com.ciai.vetclinic.security.IsAllowedVetOrAdminOrClient
import com.ciai.vetclinic.service.VetService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@Api(value = "VetClinic Management System - Vet API",
        description = "Management operations for Vets in the IADI 2019 PET Clinic")

@RestController
@RequestMapping("/api")
class VetController(private val vets: VetService) {


    @ApiOperation(value = "Hire a new vet")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully hired a new vet "),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PostMapping("/vets")
    @IsAdmin
    fun hireNewVet(@Valid @RequestBody newVet: NewVetDTO): VetDTO =
            vets.hireNewVet(newVet)



    @ApiOperation(value = "View a list of all vets", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all vets"),
        ApiResponse(code = 404, message = "The vets does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets")
    @IsAdmin
    fun getAllVets(): List<VetDTO> =
            vets.getAllVets()


    @ApiOperation(value = "Get a vet by id", response = VetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved vet details"),
        ApiResponse(code = 404, message = "The vet with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{id}")
    @IsAllowedVetOrAdmin
    fun getOneVet(@PathVariable("id") vetId: Long): VetDTO =
            handleExceptions {
                vets.getOneVet(vetId)

            }

/*
    @ApiOperation(value = "Update vet information", response = VetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated vet information"),
        ApiResponse(code = 404, message = "The vet with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @PutMapping("/vets/{id}")
    fun updateVetById(@PathVariable(value = "id") vetId: Long,
                      @Valid @RequestBody newVet: UpdateVetDTO): VetDTO =
            handleExceptions {
                vets.updateVetById(vetId, newVet)
            }

 */


    @ApiOperation(value = "Fire vet", response = VetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully fired vet"),
        ApiResponse(code = 404, message = "The vet with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource")
    ])
    @DeleteMapping("/vets/{id}")
    @IsAdmin
    fun fireVet(@PathVariable(value = "id") vetId: Long): VetDTO =
            handleExceptions {
                    vets.fireVet(vetId)
            }


    @ApiOperation(value = "Get the list of appointments scheduled for a vet", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved appointment details"),
        ApiResponse(code = 404, message = "The veterinarian with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{id}/appointments")
    @IsAllowedVetOrAdmin
    fun getVetAppointmentsById(@PathVariable(value = "id") vetId: Long): List<AppointmentDTO> =
            handleExceptions {
                vets.getAllAppointmentsOfVet(vetId)
            }

    @ApiOperation(value = "Get the list of a vet's upcoming appointments", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved upcoming appointment details"),
        ApiResponse(code = 404, message = "The veterinarian with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{id}/appointments/upcoming")
    @IsAllowedVetOrAdmin
    fun getUpcomingAppointmentsOfVet(@PathVariable(value = "id") vetId: Long): List<AppointmentDTO> =
            handleExceptions {
                vets.getUpcomingAppointmentsOfVet(vetId)
            }


    @ApiOperation(value = "Get the list of a vet's completed appointments", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved completed appointment details"),
        ApiResponse(code = 404, message = "The veterinarian with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{id}/appointments/completed")
    @IsAllowedVetOrAdmin
    fun getCompletedAppointmentsOfVet(@PathVariable(value = "id") vetId: Long): List<AppointmentDTO> =
            handleExceptions {
                vets.getCompletedAppointmentsOfVet(vetId)
            }


    @ApiOperation(value = "Mark a vet's accepted appointment as completed", response = AppointmentDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully marked appointment as completed"),
        ApiResponse(code = 400, message = "The appointment has already been marked as completed"),
        ApiResponse(code = 404, message = "The veterinarian/appointment with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("/vets/{id}/appointments/completeAppointment")
    @IsAllowedVet
    fun completeAppointment(@PathVariable(value = "id") vetId: Long,
                            @Valid @RequestBody appointment: CompleteAppointmentDTO): AppointmentDTO =
            handleExceptions {
                vets.completeAppointmentOfVet(vetId, appointment)
            }


    @ApiOperation(value = "Get the list of a vet's shifts for the month AKA its schedule", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved schedule"),
        ApiResponse(code = 404, message = "The veterinarian with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{id}/schedule")
    @IsAllowedVetOrAdmin
    fun getScheduleOfVet(@PathVariable(value = "id") vetId: Long): List<TimeSlotDTO> =
            handleExceptions {
                vets.getScheduleOfVet(vetId)
            }

    @ApiOperation(value = "Get the available slots of a vet", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved available slots of vet"),
        ApiResponse(code = 404, message = "The veterinarian with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{id}/timeslots/available")
    @IsAllowedVetOrAdmin
    fun getFreeSlotsOfVet(@PathVariable(value = "id") vetId: Long): List<TimeSlotDTO> =
            handleExceptions {
                vets.getAvailableSlotsOfVet(vetId)
            }


    @ApiOperation(value = "Set a vet's schedule for the month")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully set the vet's schedule"),
        ApiResponse(code = 404, message = "The veterinarian with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("/vets/{id}/schedule")
    @IsAdmin
    fun setScheduleOfVet(@PathVariable(value = "id") vetId: Long,
                         @Valid @RequestBody newSchedule: NewScheduleDTO) =
            handleExceptions {
                vets.setScheduleOfVet(vetId, newSchedule)
            }


    @ApiOperation(value = "Get a time slot of a vet", response = TimeSlotDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved slot"),
        ApiResponse(code = 404, message = "The veterinarian/slot with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/vets/{vetId}/schedule/{slotId}")
    @IsAllowedVetOrAdminOrClient
    fun getSlotOfVetById(@PathVariable(value = "vetId") vetId: Long,
                         @PathVariable(value = "slotId") slotId: Long): TimeSlotDTO =
            handleExceptions {
                vets.getSlotOfVetById(vetId, slotId)
            }


    @ApiOperation(value = "Edit a time slot of a vet", response = TimeSlotDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully edited slot"),
        ApiResponse(code = 404, message = "The veterinarian/slot with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/vets/{vetId}/schedule/{slotId}")
    @IsAdmin
    fun editSlotOfVetById(@PathVariable(value = "vetId") vetId: Long,
                          @PathVariable(value = "slotId") slotId: Long,
                          @Valid @RequestBody editSlotDTO: EditTimeSlotDTO): TimeSlotDTO =
            handleExceptions {
                vets.editSlotOfVetById(vetId, slotId, editSlotDTO)
            }


    @ApiOperation(value = "Delete a time slot of a vet", response = TimeSlotDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted slot"),
        ApiResponse(code = 404, message = "The veterinarian/slot with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @DeleteMapping("/vets/{vetId}/schedule/{slotId}")
    @IsAdmin
    fun deleteSlotOfVetById(@PathVariable(value = "vetId") vetId: Long,
                            @PathVariable(value = "slotId") slotId: Long) =
            handleExceptions {
                vets.deleteSlotOfVetById(vetId, slotId)
            }

}