package com.ciai.vetclinic.controller

import com.ciai.vetclinic.controller.exceptions.handleExceptions
import com.ciai.vetclinic.model.appointment.AppointmentDTO
import com.ciai.vetclinic.model.appointment.NewAppointmentDTO
import com.ciai.vetclinic.model.user.client.ClientDTO
import com.ciai.vetclinic.model.pet.PetDTO
import com.ciai.vetclinic.model.user.vet.VetDTO
import com.ciai.vetclinic.security.IsAdmin
import com.ciai.vetclinic.service.AppointmentService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(value = "VetClinic Management System - Appointment API",
        description = "Management operations for Appointments in the IADI 2019 PET Clinic")

@RestController
@RequestMapping("/api")
class AppointmentController(val appointments: AppointmentService) {

    @ApiOperation(value = "Schedule a new appointment", response = AppointmentDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Appointment scheduled successfully"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "The timeSlot already has an appointment")
    ])
    @PostMapping("/appointments")
    fun newAppointment(@Valid @RequestBody newAppointment: NewAppointmentDTO): AppointmentDTO =
            appointments.newAppointment(newAppointment)


    @ApiOperation(value = "View a list of all appointments", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Appointment retrieved appointments details"),
        ApiResponse(code = 404, message = "The veterinarian/pet with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/appointments")
    @IsAdmin
    fun getAllAppointments(): List<AppointmentDTO> =
            appointments.getAllAppointments()


    @ApiOperation(value = "Get the details of an appointment by id", response = AppointmentDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved appointment details"),
        ApiResponse(code = 404, message = "The appointment with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/appointments/{id}")
    @IsAdmin
    fun getAppointmentById(@PathVariable(value = "id") appointmentId: Long): AppointmentDTO =
            handleExceptions {
                appointments.getAppointmentById(appointmentId)
            }


    @ApiOperation(value = "Get the vet from an appointment by appointment id", response = VetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved the vet details"),
        ApiResponse(code = 404, message = "The appointment with the supplied id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/appointments/{id}/vet")
    @IsAdmin
    fun getAppointmentVetById(@PathVariable(value = "id") appointmentId: Long): VetDTO =
            handleExceptions {
                appointments.getAppointmentVetById(appointmentId)
            }


    @ApiOperation(value = "Get the client from an appointment by appointment id", response = ClientDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved the client details"),
        ApiResponse(code = 404, message = "The client with the supplied appointment id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/appointments/{id}/client")
    @IsAdmin
    fun getAppointmentClientById(@PathVariable(value = "id") appointmentId: Long): ClientDTO =
            handleExceptions {
                appointments.getAppointmentClientById(appointmentId)
            }


    @ApiOperation(value = "Get the pet from an appointment by appointment id", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved the pet details"),
        ApiResponse(code = 404, message = "The pet with the supplied appointment id does not exist"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/appointments/{id}/pet")
    @IsAdmin
    fun getAppointmentPetById(@PathVariable(value = "id") appointmentId: Long): PetDTO =
            handleExceptions {
                appointments.getAppointmentPetById(appointmentId)
            }
}
