package com.ciai.vetclinic.model.user.client

import com.ciai.vetclinic.model.user.IUpdatableUserDTO
import com.ciai.vetclinic.model.user.Role


// For creating new clients
data class ClientDTO(
        var id: Long,
        var name: String,
        var picture: String,
        var email: String,
        var phoneNumber: String,
        var address: String,
        var username: String,
        var password: String,
        var role: Role
) {
    constructor(client: Client) : this(
            client.id,
            client.name,
            client.picture,
            client.email,
            client.phoneNumber,
            client.address,
            client.username,
            client.password,
            client.role
    )
}

//Used to create new client
data class NewClientDTO(
        var name: String,
        var picture: String,
        var email: String,
        var phoneNumber: String,
        var address: String,
        var username: String,
        var password: String
)

//Used to update client info
data class UpdateClientDTO(
        override var name: String,
        override var picture: String,
        override var email: String,
        override var phoneNumber: String,
        override var address: String,
        override var username: String,
        override var password: String,
        override var role: Role
) : IUpdatableUserDTO