package com.ciai.vetclinic.model.pet


// Transfer the whole DAO
data class PetDTO(
        var id: Long = 0,
        var name: String = "",
        var species: String = "",
        var age: Int = 0,
        var picture: String = "", // Should we use String?
        var physicalDescription: String = "",
        var notes: String = "",
        var medicalRecord: String = "",
        //var ownerId: Long TODO: implement getPetOwner endpoint
        var removed: Boolean = false
) {
    constructor(pet: Pet) : this(
            pet.id,
            pet.name,
            pet.species,
            pet.age,
            pet.picture,
            pet.physicalDescription,
            pet.notes,
            pet.medicalRecord,
            //pet.owner.id
            false
    )
}

// For creating a new Pet
data class NewPetDTO(
        var name: String = "",
        var species: String = "",
        var age: Int = 0,
        var picture: String = "",
        var physicalDescription: String = "",
        var notes: String = "",
        var medicalRecord: String = "",
        var ownerId: Long = 0
)


// For updating an existing Pet
data class UpdatePetDTO(
        var name: String,
        var species: String,
        var age: Int,
        var picture: String,
        var physicalDescription: String,
        var notes: String,
        var medicalRecord: String
)