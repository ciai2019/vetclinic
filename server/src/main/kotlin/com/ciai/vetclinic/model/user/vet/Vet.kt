package com.ciai.vetclinic.model.user.vet

import com.ciai.vetclinic.model.user.Role
import com.ciai.vetclinic.model.user.User
import com.ciai.vetclinic.model.appointment.Appointment
import com.ciai.vetclinic.model.timeslot.TimeSlot
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.OneToMany
import javax.persistence.PrimaryKeyJoinColumn

@Entity
@PrimaryKeyJoinColumn(name = "vetId")
data class Vet(
        var frozenAccount: Boolean = false,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "vet")
        @JsonIgnoreProperties("vet")
        var appointments: MutableList<Appointment> = mutableListOf(),

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "vet")
        var schedule: MutableList<TimeSlot> = mutableListOf()

) : User(name = "", picture = "", email = "", phoneNumber = "", address = "", username = "", password = "", role = Role.ROLE_VET) {

    // Used when creating a new Vet
    constructor(newVetDTO: NewVetDTO) : this() {
        super.name = newVetDTO.name
        super.picture = newVetDTO.picture
        super.email = newVetDTO.email
        super.phoneNumber = newVetDTO.phoneNumber
        super.address = newVetDTO.address
        super.username = newVetDTO.username
        super.password = newVetDTO.password
        super.role = Role.ROLE_VET
    }

    fun fire() {
        this.frozenAccount = true
    }
}