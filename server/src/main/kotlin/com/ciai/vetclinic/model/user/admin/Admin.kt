package com.ciai.vetclinic.model.user.admin

import com.ciai.vetclinic.model.user.Role
import com.ciai.vetclinic.model.user.User
import javax.persistence.*

@Entity
@PrimaryKeyJoinColumn(name = "adminId")
data class Admin(
        var isDefault: Boolean
)

 : User(name = "", picture = "", email = "", phoneNumber = "", address = "", username = "", password = "", role = Role.ROLE_ADMIN){

    // Used for creating a default admin
    constructor(): this(true){
        super.username = "root"
        super.password = "root"
        super.role = Role.ROLE_ADMIN
    }

    // Used when creating a new Admin
    constructor(newAdminDTO: NewAdminDTO) : this(false){

            super.name = newAdminDTO.name
            super.picture = newAdminDTO.picture
            super.email = newAdminDTO.email
            super.phoneNumber = newAdminDTO.phoneNumber
            super.address = newAdminDTO.address
            super.username = newAdminDTO.username
            super.password = newAdminDTO.password
            super.role = Role.ROLE_ADMIN

    }
}
