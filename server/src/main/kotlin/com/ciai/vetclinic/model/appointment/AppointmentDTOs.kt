package com.ciai.vetclinic.model.appointment

import com.ciai.vetclinic.model.timeslot.SimpleTimeSlotDTO


// For transferring appointment data
data class AppointmentDTO(
        val id: Long,
        val timeSlot: SimpleTimeSlotDTO,
        val completed: Boolean,
        val vetId: Long,
        val clientId: Long,
        val petId: Long,
        val description: String
) {
    constructor(appointment: Appointment) : this(
            appointment.id,
            SimpleTimeSlotDTO(appointment.timeSlot),
            appointment.completed,
            appointment.vet.id,
            appointment.client.id,
            appointment.pet.id,
            appointment.description
    )
}


// For creating new appointments
data class NewAppointmentDTO(
        val vetId: Long,
        val petId: Long,
        val slotId: Long
)

// DTO received when the vet marks an appointment as completed
data class CompleteAppointmentDTO(
        val id: Long,
        val description: String
)