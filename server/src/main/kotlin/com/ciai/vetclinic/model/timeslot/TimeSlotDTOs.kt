package com.ciai.vetclinic.model.timeslot


import java.time.LocalDateTime

data class TimeSlotDTO(
        val id: Long = 0,
        val startDate: LocalDateTime,
        val finishDate: LocalDateTime,
        val vetId: Long,
        val appointmentId: Long?
) {

    // Create a new timeSlot DAO from NewTimeSlotDTO
    constructor(timeSlot: TimeSlot) : this(
            timeSlot.id,
            timeSlot.startDate,
            timeSlot.endDate,
            timeSlot.vet.id,
            if(timeSlot.hasAppointment()) timeSlot.appointment!!.id else null
    )

}

data class AvailableTimeSlotDTO(
        val id: Long = 0,
        val startDate: LocalDateTime,
        val finishDate: LocalDateTime,
        val vetId: Long
) {
    constructor(timeSlot: TimeSlot) : this(
            timeSlot.id,
            timeSlot.startDate,
            timeSlot.endDate,
            timeSlot.vet.id
    )
}

data class SimpleTimeSlotDTO(
        val startDate: LocalDateTime,
        val finishDate: LocalDateTime
) {
    constructor(timeSlot: TimeSlot) : this(
            timeSlot.startDate,
            timeSlot.endDate
    )
}

data class NewTimeSLotDTO(
        val startDate: LocalDateTime,
        val endDate: LocalDateTime
)

data class EditTimeSlotDTO(
        val startDate: LocalDateTime,
        val endDate: LocalDateTime,
        val vetId: Long
)

data class NewScheduleDTO(
        val firstDate: LocalDateTime,
        val lastDate: LocalDateTime,
        val timeSlots: List<NewTimeSLotDTO>
)