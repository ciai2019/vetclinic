package com.ciai.vetclinic.model.user

data class EmployeeDTO(
        val id: Long,
        val name: String,
        val picture: String,
        val email: String,
        val phoneNumber: String,
        val role: Role
){
    constructor(user: User) : this(
            user.id,
            user.name,
            user.picture,
            user.email,
            user.phoneNumber,
            user.role
    )
}

data class UserDTO(
        val name: String,
        val picture: String,
        val email: String,
        val phoneNumber: String,
        val address: String
){
    constructor(user: User): this(
            user.name,
            user.picture,
            user.email,
            user.phoneNumber,
            user.address
    )
}

data class UpdateUserDTO(
        val name: String,
        val picture: String,
        val email: String,
        val phoneNumber: String,
        val address: String,
        val username: String,
        val password: String
){
    constructor(user: User): this(
            user.name,
            user.picture,
            user.email,
            user.phoneNumber,
            user.address,
            user.username,
            user.password
    )
}
data class UserIdDTO (
        var userId :Long
)