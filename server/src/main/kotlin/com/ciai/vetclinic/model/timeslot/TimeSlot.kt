package com.ciai.vetclinic.model.timeslot

import com.ciai.vetclinic.model.appointment.Appointment
import com.ciai.vetclinic.model.user.vet.Vet
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class TimeSlot (

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0,

    var startDate: LocalDateTime,

    var endDate: LocalDateTime,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vet_id", nullable = false)
    var vet: Vet,
/*
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "shift_id", nullable = false)
    val shift: Shift,

 */


    @OneToOne (fetch = FetchType.LAZY, cascade = [CascadeType.ALL], mappedBy = "timeSlot")
    var appointment: Appointment?
    //var hasAppointment: Boolean = false

) {

    constructor(newTimeSLotDTO: NewTimeSLotDTO, vet: Vet): this(
            0,
            newTimeSLotDTO.startDate,
            newTimeSLotDTO.endDate,
            vet,
            null

    )

    fun hasAppointment(): Boolean {
        return appointment != null
    }

    fun edit(editTimeSlotDTO: EditTimeSlotDTO, vet: Vet){
        this.startDate = editTimeSlotDTO.startDate
        this.endDate = editTimeSlotDTO.endDate
        this.vet = vet
    }



}