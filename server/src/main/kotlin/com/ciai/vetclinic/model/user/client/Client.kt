package com.ciai.vetclinic.model.user.client

import com.ciai.vetclinic.model.user.Role
import com.ciai.vetclinic.model.user.User
import com.ciai.vetclinic.model.appointment.Appointment
import com.ciai.vetclinic.model.pet.Pet
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.OneToMany
import javax.persistence.PrimaryKeyJoinColumn

@Entity
@PrimaryKeyJoinColumn(name = "clientId")
data class Client(

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
        @JsonIgnoreProperties("client")
        var appointments: MutableList<Appointment> = mutableListOf(),

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
        @JsonIgnoreProperties(value = ["owner", "appointments"])
        var pets: MutableList<Pet> = mutableListOf()
) : User(name = "", picture = "", email = "", phoneNumber = "", address = "", username = "", password = "", role = Role.ROLE_CLIENT) {

    constructor(newClientDTO: NewClientDTO) : this() {
        super.name = newClientDTO.name
        super.picture = newClientDTO.picture
        super.email = newClientDTO.email
        super.phoneNumber = newClientDTO.phoneNumber
        super.address = newClientDTO.address
        super.username = newClientDTO.username
        super.password = newClientDTO.password
        super.role = Role.ROLE_CLIENT
    }

    fun update(other: UpdateClientDTO) {
        super.name = other.name
        super.picture = other.picture
        super.email = other.email
        super.phoneNumber = other.phoneNumber
        super.address = other.address
        super.username = other.username
        super.password = other.password
        //super.role = other.role
    }
}
