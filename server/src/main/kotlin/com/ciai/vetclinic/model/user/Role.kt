package com.ciai.vetclinic.model.user

enum class Role {
    USER,
    ROLE_ADMIN,
    ROLE_VET,
    ROLE_CLIENT
}