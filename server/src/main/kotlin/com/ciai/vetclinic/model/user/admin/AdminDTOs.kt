package com.ciai.vetclinic.model.user.admin

import com.ciai.vetclinic.model.user.IUpdatableUserDTO
import com.ciai.vetclinic.model.user.Role

// For transferring Vet data
data class AdminDTO(
        val isDefault: Boolean,
        val id: Long,
        val name: String,
        val picture: String,
        val email: String,
        val phoneNumber: String,
        val address: String,
        val username: String,
        val password: String
) {

    constructor(admin: Admin) : this(
            admin.isDefault,
            admin.id,
            admin.name,
            admin.picture,
            admin.email,
            admin.phoneNumber,
            admin.address,
            admin.username,
            admin.password
    )
}

// For creating/updating Admins
data class NewAdminDTO(
        val name: String,
        val picture: String,
        val email: String,
        val phoneNumber: String,
        val address: String,
        val username: String,
        val password: String
)

data class UpdateAdminDTO(
        override var name: String,
        override var picture: String,
        override var email: String,
        override var phoneNumber: String,
        override var address: String,
        override var username: String,
        override var password: String,
        override var role: Role
) : IUpdatableUserDTO