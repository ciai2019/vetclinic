package com.ciai.vetclinic.model.user.vet

import com.ciai.vetclinic.model.user.IUpdatableUserDTO
import com.ciai.vetclinic.model.user.Role


// For transferring Vet data
data class VetDTO(
        var id: Long,
        var name: String,
        var picture: String,
        var email: String,
        var phoneNumber: String,
        var address: String,
        var username: String,
        //var password: String,
        //var role: Role,
        var frozenAccount: Boolean

) {

    constructor(vet: Vet) : this(
            vet.id,
            vet.name,
            vet.picture,
            vet.email,
            vet.phoneNumber,
            vet.address,
            vet.username,
            //vet.password,
            //vet.role,
            vet.frozenAccount
    )
}

// For creating/updating Vets
data class NewVetDTO(
        var name: String,
        var picture: String,
        var email: String,
        var phoneNumber: String,
        var address: String,
        var username: String,
        var password: String
)

data class UpdateVetDTO(
        override var name: String,
        override var picture: String,
        override var email: String,
        override var phoneNumber: String,
        override var address: String,
        override var username: String,
        override var password: String,
        override var role: Role
) : IUpdatableUserDTO