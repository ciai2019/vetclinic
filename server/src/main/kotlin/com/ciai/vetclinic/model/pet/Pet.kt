package com.ciai.vetclinic.model.pet

import com.ciai.vetclinic.model.appointment.Appointment
import com.ciai.vetclinic.model.user.client.Client
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
data class Pet(

        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,
        var name: String = "",
        var species: String = "",
        var age: Int = 0,
        var picture: String = "", // Should we use String?
        var physicalDescription: String = "",
        var notes: String = "",
        var medicalRecord: String = "",

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "owner_id")
        @JsonIgnoreProperties("pets")
        var owner: Client,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "pet")
        @JsonIgnoreProperties("pet")
        var appointments: MutableList<Appointment> = mutableListOf(),

        var removed: Boolean = false
) {

    // Create a new pet DAO from NewPetDTO
    constructor(pet: NewPetDTO, owner: Client) : this(
            0,
            pet.name,
            pet.species,
            pet.age,
            pet.picture,
            pet.physicalDescription,
            pet.notes,
            pet.medicalRecord,
            owner,
            mutableListOf<Appointment>(),
            false
    )

    fun update(other: UpdatePetDTO) {
        this.name = other.name
        this.species = other.species
        this.age = other.age
        this.picture = other.picture
        this.physicalDescription = other.physicalDescription
        this.notes = other.notes
        this.medicalRecord = other.medicalRecord
    }

    fun markAsRemoved() {
        this.removed = true
    }
}