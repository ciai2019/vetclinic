package com.ciai.vetclinic.model.appointment

import com.ciai.vetclinic.model.user.client.Client
import com.ciai.vetclinic.model.pet.Pet
import com.ciai.vetclinic.model.timeslot.TimeSlot
import com.ciai.vetclinic.model.user.vet.Vet
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

@Entity
data class Appointment (
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @OneToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "slot_id", nullable = false)
        var timeSlot: TimeSlot,

        var completed: Boolean = false,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "vet_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        //@JsonIgnoreProperties("appointments")
        var vet: Vet,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "client_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        //@JsonIgnoreProperties("appointments")
        var client: Client,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "pet_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        //@JsonIgnoreProperties(value = ["appointments", "owner"])
        var pet: Pet,

        var description: String = ""

){
        constructor(timeSlot: TimeSlot, vet: Vet, pet: Pet) : this(
                0,
                timeSlot,
                false,
                vet,
                pet.owner,
                pet,
                ""
        )

        fun complete(description: String){
                this.completed = true
                this.description = description
        }
}