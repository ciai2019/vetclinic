package com.ciai.vetclinic.model.user

import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
open class User(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,
        open var name: String,
        open var picture: String,
        open var email: String,
        open var phoneNumber: String,
        open var address: String,

        @Column(unique = true)
        open var username: String,

        open var password: String,

        @Enumerated(EnumType.STRING)
        open var role: Role

) {

    fun update(other: UpdateUserDTO) {
        this.name = other.name
        this.picture = other.picture
        this.email = other.email
        this.phoneNumber = other.phoneNumber
        this.address = other.address
        this.username = other.username
        this.password = other.password
    }
}


interface IUpdatableUserDTO {
    var name: String
    var picture: String
    var email: String
    var phoneNumber: String
    var address: String
    var username: String
    var password: String
    var role: Role
}

