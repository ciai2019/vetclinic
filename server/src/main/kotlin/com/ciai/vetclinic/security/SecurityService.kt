package com.ciai.vetclinic.security

import com.ciai.vetclinic.config.CustomUserDetails
import com.ciai.vetclinic.config.CustomUserDetailsService
import com.ciai.vetclinic.config.UserAuthToken
import com.ciai.vetclinic.model.pet.NewPetDTO
import com.ciai.vetclinic.model.user.User
import com.ciai.vetclinic.model.user.client.Client
import com.ciai.vetclinic.model.user.vet.Vet
import com.ciai.vetclinic.repository.ClientRepository
import com.ciai.vetclinic.repository.PetRepository
import com.ciai.vetclinic.repository.UsersRepository
import com.ciai.vetclinic.repository.VetRepository
import com.ciai.vetclinic.service.exceptions.UserNotFoundServiceException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component("SecurityService")
class SecurityService {

    @Autowired
    lateinit var users: UsersRepository

    @Autowired
    lateinit var clients: ClientRepository

    @Autowired
    lateinit var vets: VetRepository

    @Autowired
    lateinit var pets: PetRepository

    private fun getPrincipalUserDAO(username: String): User =
            users.findByUsername(username).orElseThrow { UserNotFoundServiceException(-1, username) }

    fun isAllowedUser(principal: UserAuthToken, userId: Long): Boolean {
        val user = users.findById(userId)
        val principalUser = getPrincipalUserDAO(principal.name)

        return user.isPresent && principalUser.id == user.get().id
    }

    fun isAllowedUserByUsername(principal: UserAuthToken, username: String): Boolean {
        val user = users.findByUsername(username)
        val principalUser = getPrincipalUserDAO(principal.name)

        return user.isPresent && principalUser.id == user.get().id
    }

    fun isAllowedClient(principal: UserAuthToken, clientId: Long): Boolean {
        val client = clients.findById(clientId)
        val principalUser = getPrincipalUserDAO(principal.name)

        return client.isPresent && principalUser.id == client.get().id
    }

    fun isAllowedVet(principal: UserAuthToken, vetId: Long): Boolean {
        val vet = vets.findById(vetId)
        val principalUser = getPrincipalUserDAO(principal.name)

        return vet.isPresent && principalUser.id == vet.get().id
    }

    fun isAllowedOwner(principal: UserAuthToken, petId: Long): Boolean {
        val pet = pets.findById(petId)
        val principalUser = getPrincipalUserDAO(principal.name)

        return pet.isPresent && principalUser.id == pet.get().owner.id
    }

    fun isAllowedForAddNewPet(principal: UserAuthToken, newPetDTO: NewPetDTO): Boolean {
        val principalUser = getPrincipalUserDAO(principal.name)
        return principalUser.id == newPetDTO.ownerId
    }
}