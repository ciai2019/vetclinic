package com.ciai.vetclinic.security

import org.springframework.security.access.prepost.PreAuthorize

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("hasRole('ADMIN')")
annotation class IsAdmin


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedUser(principal, #userId)")
annotation class IsAllowedUser

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedUserByUsername(principal, #username)")
annotation class IsAllowedUserByUsername


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("hasRole('CLIENT')")
annotation class IsClient


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedClient(principal, #clientId)")
annotation class IsAllowedClient


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedClient(principal, #clientId) or hasRole('ADMIN')")
annotation class IsAllowedClientOrAdmin


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedVet(principal, #vetId)")
annotation class IsAllowedVet


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedVet(principal, #vetId) or hasRole('ADMIN')")
annotation class IsAllowedVetOrAdmin


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedVet(principal, #vetId) or hasRole('ADMIN') or hasRole('CLIENT')")
annotation class IsAllowedVetOrAdminOrClient


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedForAddNewPet(principal, #newPetDTO) and hasRole('CLIENT')")
annotation class IsAllowedForAddNewPet


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedOwner(principal, #petId)")
annotation class IsAllowedPetOwner



@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedOwner(principal, #petId) or hasRole('ADMIN') or hasRole('VET')")
annotation class IsAllowedPetOwnerOrAdminOrVet

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("hasRole('ADMIN') or hasRole('VET')")
annotation class IsAdminOrVet

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
annotation class IsAdminOrClient

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@PreAuthorize("@SecurityService.isAllowedClient(principal, #clientId) or hasRole('ADMIN') or hasRole('VET')")
annotation class IsAllowedClientOrAdminOrVet