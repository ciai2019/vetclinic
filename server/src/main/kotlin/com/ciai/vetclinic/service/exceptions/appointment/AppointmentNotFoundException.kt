package com.ciai.vetclinic.service.exceptions.appointment

import com.ciai.vetclinic.service.exceptions.NotFoundServiceException

class AppointmentNotFoundException (appointmentId: Long) : NotFoundServiceException ("There is no appointment with id $appointmentId")
