package com.ciai.vetclinic.service.exceptions.appointment

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class AppointmentAlreadyCompletedException (appointmentId: Long) : BadRequestServiceException("The appointment with id $appointmentId has already been completed")
