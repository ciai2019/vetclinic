package com.ciai.vetclinic.service

import com.ciai.vetclinic.model.appointment.Appointment
import com.ciai.vetclinic.model.appointment.AppointmentDTO
import com.ciai.vetclinic.model.appointment.NewAppointmentDTO
import com.ciai.vetclinic.model.user.client.ClientDTO
import com.ciai.vetclinic.model.pet.Pet
import com.ciai.vetclinic.model.pet.PetDTO
import com.ciai.vetclinic.model.timeslot.TimeSlot
import com.ciai.vetclinic.model.user.vet.Vet
import com.ciai.vetclinic.model.user.vet.VetDTO
import com.ciai.vetclinic.repository.AppointmentRepository
import com.ciai.vetclinic.repository.PetRepository
import com.ciai.vetclinic.repository.SlotRepository
import com.ciai.vetclinic.repository.VetRepository
import com.ciai.vetclinic.service.exceptions.appointment.AppointmentNotFoundException
import com.ciai.vetclinic.service.exceptions.pet.PetNotFoundException
import com.ciai.vetclinic.service.exceptions.slot.SlotAlreadyHasAppointmentException
import com.ciai.vetclinic.service.exceptions.slot.SlotDoesNotBelongToVetException
import com.ciai.vetclinic.service.exceptions.slot.SlotNotFoundException
import com.ciai.vetclinic.service.exceptions.vet.VetNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AppointmentService {

    @Autowired
    lateinit var appointments: AppointmentRepository

    @Autowired
    lateinit var vets: VetRepository

    @Autowired
    lateinit var pets: PetRepository

    @Autowired
    lateinit var slots: SlotRepository

    private fun getAppointmentDAO(appointmentId: Long): Appointment =
            appointments.findById(appointmentId).orElseThrow { AppointmentNotFoundException(appointmentId) }

    private fun getVetDAO(vetId: Long): Vet =
            vets.findById(vetId).orElseThrow { VetNotFoundException(vetId) }

    private fun getPetDAO(petId: Long): Pet {
        val pet = pets.findById(petId)

        if (!pet.isPresent || pet.get().removed)
            throw PetNotFoundException(petId)

        return pet.get()
    }

    private fun getTimeSlotDAO(slotId: Long): TimeSlot {
        return slots.findById(slotId).orElseThrow { SlotNotFoundException(slotId) }
    }


    fun newAppointment(newAppointment: NewAppointmentDTO): AppointmentDTO {
        val timeSlot: TimeSlot = getTimeSlotDAO(newAppointment.slotId)
        val vet: Vet = getVetDAO(newAppointment.vetId)
        val pet: Pet = getPetDAO(newAppointment.petId)


        if(timeSlot.vet.id != vet.id)
            throw SlotDoesNotBelongToVetException(timeSlot.id, timeSlot.vet.id)


        if(timeSlot.hasAppointment())
            throw SlotAlreadyHasAppointmentException(timeSlot.id)

        val a = Appointment(timeSlot, vet, pet)
        //timeSlot.addAppointment(a)
        //slots.save(timeSlot)
        return AppointmentDTO(appointments.save(a))
    }

    fun getAllAppointments(): List<AppointmentDTO> =
            appointments.findAll().map { appointment -> AppointmentDTO(appointment) }


    fun getAppointmentById(id: Long): AppointmentDTO =
            AppointmentDTO(getAppointmentDAO(id))


    fun getAppointmentVetById(id: Long): VetDTO =
            VetDTO(getAppointmentDAO(id).vet)

    fun getAppointmentClientById(id: Long): ClientDTO =
            ClientDTO(getAppointmentDAO(id).client)

    fun getAppointmentPetById(id: Long): PetDTO =
            PetDTO(getAppointmentDAO(id).pet)

}