package com.ciai.vetclinic.service

import com.ciai.vetclinic.config.UserAuthToken
import com.ciai.vetclinic.model.appointment.Appointment
import com.ciai.vetclinic.model.appointment.AppointmentDTO
import com.ciai.vetclinic.model.appointment.CompleteAppointmentDTO
import com.ciai.vetclinic.model.timeslot.*
import com.ciai.vetclinic.model.user.vet.NewVetDTO
import com.ciai.vetclinic.model.user.vet.UpdateVetDTO
import com.ciai.vetclinic.model.user.vet.Vet
import com.ciai.vetclinic.model.user.vet.VetDTO
import com.ciai.vetclinic.repository.AppointmentRepository
import com.ciai.vetclinic.repository.SlotRepository
import com.ciai.vetclinic.repository.UsersRepository
import com.ciai.vetclinic.repository.VetRepository
import com.ciai.vetclinic.service.exceptions.appointment.AppointmentAlreadyCompletedException
import com.ciai.vetclinic.service.exceptions.appointment.AppointmentNotFoundException
import com.ciai.vetclinic.service.exceptions.slot.SlotAlreadyHasAppointmentException
import com.ciai.vetclinic.service.exceptions.slot.SlotNotFoundException
import com.ciai.vetclinic.service.exceptions.vet.VetNotFoundException
import com.ciai.vetclinic.service.exceptions.vet.schedule.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.temporal.WeekFields
import java.util.*

@Service
class VetService {

    private companion object {
        const val MIN_WORK_MINS: Long = 160 * 60 // 160 hours
        const val MAX_SHIFT_DURATION: Long = 12 * 60 // 12 hours
        const val MIN_SHIFT_MINS_TO_REST: Long = 6 * 60 // 6 hours
        const val MIN_REST_TIME: Long = 8 * 60 // 8 hours
        const val MAX_MINS_PER_WEEK: Long = 40 * 60 // 40 hours
        const val SLOT_DURATION_MINS: Long = 30 // 30 mins
    }

    @Autowired
    lateinit var vets: VetRepository

    @Autowired
    lateinit var users: UsersService

    @Autowired
    lateinit var appointments: AppointmentRepository


    @Autowired
    lateinit var slots: SlotRepository

    private fun getVetDAO(vetId: Long): Vet {
        return vets.findById(vetId).orElseThrow { VetNotFoundException(vetId) }
    }

    private fun getAppointmentDAO(appointmentId: Long): Appointment {
        return appointments.findById(appointmentId).orElseThrow { AppointmentNotFoundException(appointmentId) }
    }

    private fun getSlotDAOOfVetById(vetId: Long, slotId: Long): TimeSlot{
        val vet = getVetDAO(vetId)

        return slots.getByIdAndVetId(slotId, vet.id).orElseThrow { SlotNotFoundException(slotId) }
    }


    fun getAllVets(): List<VetDTO> {
        return vets.findAll().map { vet -> VetDTO(vet) }
    }

    fun getOneVet(vetId: Long): VetDTO =
            VetDTO(getVetDAO(vetId))

    fun hireNewVet(newVetDTO: NewVetDTO): VetDTO {
        val newVet = Vet(newVetDTO)
        users.addUser(newVet)

        val auth = UserAuthToken(
                newVet.username,
                newVet.password,
                mutableListOf(SimpleGrantedAuthority(newVet.role.name))
        )

        SecurityContextHolder.getContext().authentication = auth

        return VetDTO(newVet)

    }

    /*
    fun updateVetById(vetId: Long, updatedVet: UpdateVetDTO): VetDTO {
        val originalVet = getVetDAO(vetId)
        originalVet.update(updatedVet)

        return VetDTO(vets.save(originalVet))
    }

     */

    fun fireVet(vetId: Long): VetDTO {
        val firedVet = getVetDAO(vetId)

        firedVet.fire()
        vets.save(firedVet)

        return VetDTO(firedVet)
    }

    fun getAllAppointmentsOfVet(vetId: Long): List<AppointmentDTO> =
            getVetDAO(vetId).appointments.map { appointment -> AppointmentDTO(appointment) }


    fun getUpcomingAppointmentsOfVet(vetId: Long): List<AppointmentDTO> =
            appointments.getAppointmentsByVetIdAndCompletedFalse(vetId).map { appointment ->
                AppointmentDTO(appointment)
            }


    fun getCompletedAppointmentsOfVet(vetId: Long): List<AppointmentDTO> =
            appointments.getAppointmentsByVetIdAndCompletedTrue(vetId).map { appointment ->
                AppointmentDTO(appointment)
            }


    fun completeAppointmentOfVet(vetId: Long, completeAppointment: CompleteAppointmentDTO): AppointmentDTO {
        val appointment = getAppointmentDAO(completeAppointment.id)

        if (appointment.completed) {
            throw AppointmentAlreadyCompletedException(appointment.id)
        }

        appointment.complete(completeAppointment.description)

        return AppointmentDTO(appointments.save(appointment))

    }

    fun getScheduleOfVet(vetId: Long): List<TimeSlotDTO> {
        val vet = getVetDAO(vetId)
        return vet.schedule.map { timeSlot: TimeSlot -> TimeSlotDTO(timeSlot) }
    }

    fun getAvailableSlotsOfVet(vetId: Long): List<TimeSlotDTO> {
        val vet = getVetDAO(vetId)
        val slots = vet.schedule.filter {!it.hasAppointment()};
        return slots.map { timeSlot: TimeSlot -> TimeSlotDTO(timeSlot) };
    }

    fun setScheduleOfVet(vetId: Long, newScheduleDTO: NewScheduleDTO) {
        val vet = getVetDAO(vetId)
        val dates = HashSet<LocalDateTime>()

        for (newTimeSlotDTO in newScheduleDTO.timeSlots) {
            dates.add(newTimeSlotDTO.startDate)
        }

        verifySchedule(newScheduleDTO.firstDate, newScheduleDTO.lastDate, dates)

        val schedule: List<TimeSlot> = newScheduleDTO.timeSlots.map { newTimeSLotDTO: NewTimeSLotDTO -> TimeSlot(newTimeSLotDTO, vet) }

        slots.saveAll(schedule)
/*
        vet.schedule = mutableListOf<TimeSlot>().apply { addAll(schedule) }
        vets.save(vet)

 */
    }

    fun getSlotOfVetById(vetId: Long, slotId: Long): TimeSlotDTO {
        val slot = getSlotDAOOfVetById(vetId, slotId)
        return TimeSlotDTO(slot)
    }

    fun editSlotOfVetById(vetId: Long, slotId: Long, editSlotDTO: EditTimeSlotDTO): TimeSlotDTO {
        val slot = getSlotDAOOfVetById(vetId, slotId)
        val vet = getVetDAO(editSlotDTO.vetId)

        slot.edit(editSlotDTO, vet)

        return TimeSlotDTO(slots.save(slot))
    }

    fun deleteSlotOfVetById(vetId: Long, slotId: Long) {
        val slot = getSlotDAOOfVetById(vetId, slotId)

        if(slot.hasAppointment())
            throw SlotAlreadyHasAppointmentException(slotId)

        slots.deleteById(slotId)
    }

    private fun verifySchedule(startDate: LocalDateTime, endDate: LocalDateTime, dates: HashSet<LocalDateTime>) {
        if(!dates.contains(startDate) || !dates.contains(endDate.minusMinutes(SLOT_DURATION_MINS)))
            throw InvalidScheduleException()

        var currDate = startDate

        var totalMins = 0L
        var restRequired = false
        var lastShiftEnd = currDate

        // French locale has the same week format as Portugal
        val weekFields: WeekFields = WeekFields.of(Locale.FRANCE)
        var weekNum = startDate.get(weekFields.weekOfWeekBasedYear())
        var minsInWeek = 0L

        while (currDate < endDate) {
            var shiftMinutes = 0L

            // Check if we're in a shift
            while (dates.contains(currDate)) {
                if (restRequired) {
                    restRequired = false

                    val restTime = ChronoUnit.MINUTES.between(lastShiftEnd, currDate)

                    if (restTime < MIN_REST_TIME) {
                        throw NotEnoughRestTimeException()
                    }
                }

                val currWeekNum = currDate.get(weekFields.weekOfWeekBasedYear())

                shiftMinutes += SLOT_DURATION_MINS
                totalMins += SLOT_DURATION_MINS
                currDate = currDate.plusMinutes(SLOT_DURATION_MINS)

                // Check if we changed week
                if (weekNum != currWeekNum) {
                    if (minsInWeek > MAX_MINS_PER_WEEK)
                        throw TooManyHoursInWeekException()

                    weekNum = currWeekNum
                    minsInWeek = 0L
                }

                minsInWeek += SLOT_DURATION_MINS
            }

            when {
                shiftMinutes > MAX_SHIFT_DURATION -> throw ShiftTooLongException()
                shiftMinutes >= MIN_SHIFT_MINS_TO_REST -> {
                    restRequired = true
                    lastShiftEnd = currDate
                }
            }

            currDate = currDate.plusMinutes(SLOT_DURATION_MINS)

        }

        if (totalMins < MIN_WORK_MINS)
            throw NotEnoughHoursInScheduleException()
    }




}