package com.ciai.vetclinic.service.exceptions.slot

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class SlotAlreadyHasAppointmentException (slotId: Long) :
        BadRequestServiceException("Slot $slotId already has an appointment")