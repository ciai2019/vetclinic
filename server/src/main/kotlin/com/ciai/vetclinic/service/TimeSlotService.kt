package com.ciai.vetclinic.service

import com.ciai.vetclinic.model.timeslot.AvailableTimeSlotDTO
import com.ciai.vetclinic.model.timeslot.TimeSlot
import com.ciai.vetclinic.model.timeslot.TimeSlotDTO
import com.ciai.vetclinic.repository.SlotRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TimeSlotService {
    @Autowired
    lateinit var slotRepository: SlotRepository

    fun getAllAvailableSlots(): List<AvailableTimeSlotDTO> =
            slotRepository.getAllByAppointmentIsNull().map { timeSlot: TimeSlot -> AvailableTimeSlotDTO(timeSlot) }

    fun getAllOccupiedSlots(): List<TimeSlotDTO> =
            slotRepository.getAllByAppointmentIsNotNull().map { timeSlot: TimeSlot -> TimeSlotDTO(timeSlot) }
}