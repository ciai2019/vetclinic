package com.ciai.vetclinic.service.exceptions.vet.schedule

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class InvalidScheduleException :
        BadRequestServiceException("Invalid schedule parameters, check if time slots respect date limits")