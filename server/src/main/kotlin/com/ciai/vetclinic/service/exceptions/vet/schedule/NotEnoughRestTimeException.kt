package com.ciai.vetclinic.service.exceptions.vet.schedule

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class NotEnoughRestTimeException : BadRequestServiceException("Schedule doesn't respect minimum rest time")