package com.ciai.vetclinic.service.exceptions.admin

import com.ciai.vetclinic.service.exceptions.ForbiddenServiceException

class CannotDeleteDefaultAdminException : ForbiddenServiceException("Cannot delete the default admin")