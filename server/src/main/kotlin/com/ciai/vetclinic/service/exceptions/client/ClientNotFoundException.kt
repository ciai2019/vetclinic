package com.ciai.vetclinic.service.exceptions.client

import com.ciai.vetclinic.service.exceptions.NotFoundServiceException

class ClientNotFoundException(clientId: Long) : NotFoundServiceException("There is no client with id $clientId")
class ClientUsernameNotFoundException(username: String) : NotFoundServiceException("There is no client with username $username")