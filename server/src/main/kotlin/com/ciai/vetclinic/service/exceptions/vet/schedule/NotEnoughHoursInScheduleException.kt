package com.ciai.vetclinic.service.exceptions.vet.schedule

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class NotEnoughHoursInScheduleException : BadRequestServiceException("Every veterinarian has to work at least 160 hours per month")