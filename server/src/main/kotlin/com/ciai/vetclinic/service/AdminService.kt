package com.ciai.vetclinic.service

import com.ciai.vetclinic.config.UserAuthToken
import com.ciai.vetclinic.model.user.admin.Admin
import com.ciai.vetclinic.model.user.admin.AdminDTO
import com.ciai.vetclinic.model.user.admin.NewAdminDTO
import com.ciai.vetclinic.repository.*
import com.ciai.vetclinic.service.exceptions.admin.AdminNotFoundException

import com.ciai.vetclinic.service.exceptions.admin.CannotDeleteDefaultAdminException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class AdminService {

    @Autowired
    lateinit var admins: AdminRepository

    @Autowired
    lateinit var pets: PetRepository

    @Autowired
    lateinit var appointments: AppointmentRepository

    @Autowired
    lateinit var users: UsersService

    fun createDefaultAdminIfNotPresent(){
        val defaultAdmin = admins.getDefaultAdmin()

        if(!defaultAdmin.isPresent){
            val newDefaultAdmin = Admin()
            users.addUser(newDefaultAdmin)

            val auth = UserAuthToken(
                    newDefaultAdmin.username,
                    newDefaultAdmin.password,
                    mutableListOf(SimpleGrantedAuthority(newDefaultAdmin.role.name))
            )

            SecurityContextHolder.getContext().authentication = auth
        }
    }

    fun getAdminDAO(adminId: Long): Admin {
        return admins.findById(adminId).orElseThrow { AdminNotFoundException(adminId) }
    }

    fun hireNewAdmin(newAdminDTO: NewAdminDTO): AdminDTO {
        val newAdmin = Admin(newAdminDTO)
        users.addUser(newAdmin)

        val auth = UserAuthToken(
                newAdmin.username,
                newAdmin.password,
                mutableListOf(SimpleGrantedAuthority(newAdmin.role.name))
        )

        SecurityContextHolder.getContext().authentication = auth

        return AdminDTO(newAdmin)
    }


    /*
    fun updateAdminById(adminId: Long, updatedAdmin: UpdateAdminDTO): AdminDTO {
        val originalAdmin = getAdminDAO(adminId)
        originalAdmin.update(updatedAdmin)

        return AdminDTO(admins.save(originalAdmin))
    }

     */

    fun fireAdmin(adminId: Long) {
        val firedAdmin = getAdminDAO(adminId)

        if(firedAdmin.isDefault)
            throw CannotDeleteDefaultAdminException()

        admins.delete(firedAdmin)
    }

}
