package com.ciai.vetclinic.service

import com.ciai.vetclinic.model.user.client.Client
import com.ciai.vetclinic.model.pet.NewPetDTO
import com.ciai.vetclinic.model.pet.Pet
import com.ciai.vetclinic.model.pet.PetDTO
import com.ciai.vetclinic.model.pet.UpdatePetDTO
import com.ciai.vetclinic.repository.ClientRepository
import com.ciai.vetclinic.repository.PetRepository
import com.ciai.vetclinic.service.exceptions.client.ClientNotFoundException
import com.ciai.vetclinic.service.exceptions.pet.PetNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class PetService {

    @Autowired
    lateinit var pets: PetRepository

    @Autowired
    lateinit var clients: ClientRepository

    private fun getPetOwnerDAO(clientId: Long): Client {
        return clients.findById(clientId).orElseThrow { ClientNotFoundException(clientId) }
    }

    private fun getPetDAO(petId: Long): Pet {
        return pets.findByIdAndRemovedFalse(petId).orElseThrow { PetNotFoundException(petId) }
    }

    fun getAllPets(name: Optional<String>): List<PetDTO> {
        return if(name.isPresent){
            pets.findAllByRemovedFalse().filter { pet -> pet.name.contains(name.get(), ignoreCase = true) }.map { pet -> PetDTO(pet) }
        } else{
            pets.findAllByRemovedFalse().map { pet -> PetDTO(pet) }
        }
    }

    fun getAllRemovedPets(): List<PetDTO> = pets.findAllByRemovedTrue().map { pet -> PetDTO(pet) }

    fun addNewPet(newPet: NewPetDTO): PetDTO {
        val owner = getPetOwnerDAO(newPet.ownerId)

        return PetDTO(pets.save(Pet(newPet, owner)))
    }

    fun getOnePet(id: Long): PetDTO =
            PetDTO(getPetDAO(id))


    fun updatePet(petId: Long, newPet: UpdatePetDTO): PetDTO {
        val originalPet = getPetDAO(petId)
        originalPet.update(newPet)

        return PetDTO(pets.save(originalPet))
    }

    fun deletePet(petId: Long) {
        val pet = getPetDAO(petId)
        pet.markAsRemoved()
        pets.save(pet)
    }
}