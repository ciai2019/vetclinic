package com.ciai.vetclinic.service

import com.ciai.vetclinic.config.UserAuthToken
import com.ciai.vetclinic.model.appointment.AppointmentDTO
import com.ciai.vetclinic.model.user.client.Client
import com.ciai.vetclinic.model.user.client.ClientDTO
import com.ciai.vetclinic.model.user.client.NewClientDTO
import com.ciai.vetclinic.model.user.client.UpdateClientDTO
import com.ciai.vetclinic.model.pet.PetDTO
import com.ciai.vetclinic.model.user.EmployeeDTO
import com.ciai.vetclinic.model.user.User
import com.ciai.vetclinic.repository.ClientRepository
import com.ciai.vetclinic.service.exceptions.client.ClientNotFoundException
import com.ciai.vetclinic.service.exceptions.client.ClientUsernameNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.util.*

@Service
class ClientService {

    @Autowired
    lateinit var clients: ClientRepository

    @Autowired
    lateinit var users: UsersService

    private fun getClientDAO(clientId: Long): Client {
        return clients.findById(clientId).orElseThrow { ClientNotFoundException(clientId) }
    }

    private fun getClientDAOByUsername(clientUsername: String): Client {
        return clients.findClientByUsername(clientUsername).orElseThrow { ClientUsernameNotFoundException(clientUsername) }
    }

    fun getAllClients(): List<ClientDTO> = clients.findAll().map { client -> ClientDTO(client) }

    fun getClient(id: Long): ClientDTO =
            ClientDTO(getClientDAO(id))

    fun findUser(username: String): ClientDTO =
            ClientDTO(getClientDAOByUsername(username))

    fun createNewClient(newClientDTO: NewClientDTO): ClientDTO {
        val newClient = Client(newClientDTO)
        users.addUser(newClient)

        val auth = UserAuthToken(
                newClient.username,
                newClient.password,
                mutableListOf(SimpleGrantedAuthority(newClient.role.name))
        )

        SecurityContextHolder.getContext().authentication = auth
        return ClientDTO(newClient)
    }

    fun getClientIdByUsername(username: String) :ClientDTO { return findUser(username)}

    fun getClientAppointmentsById(id: Long): List<AppointmentDTO> {
        return getClientDAO(id).appointments.map { appointment -> AppointmentDTO(appointment) }
    }

    fun getClientPetsById(id: Long): List<PetDTO> {
        return getClientDAO(id).pets.map { pet -> PetDTO(pet) }
    }

    fun getClientPetsByUsername(userId: Long,name: Optional<String>): List<PetDTO> {
        val pets = getClientDAO(userId).pets

        return if(name.isPresent){
            pets.filter { pet -> pet.name.contains(name.get(), ignoreCase = true) }.map { pet -> PetDTO(pet) }
        } else {
            pets.map { pet -> PetDTO(pet) }
        }




    }

    /*
    fun updateClientById(id: Long, updatedClient: UpdateClientDTO): ClientDTO {
        val originalClient = getClientDAO(id)
        originalClient.update(updatedClient)

        return ClientDTO(clients.save(originalClient))
    }

     */

    fun deleteClient(clientId: Long) {
        val client = getClientDAO(clientId)
        clients.delete(client)
    }
}