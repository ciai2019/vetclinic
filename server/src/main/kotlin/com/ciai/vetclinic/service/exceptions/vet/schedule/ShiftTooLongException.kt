package com.ciai.vetclinic.service.exceptions.vet.schedule

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class ShiftTooLongException : BadRequestServiceException("Schedule has a shift with a duration longer than the limit")