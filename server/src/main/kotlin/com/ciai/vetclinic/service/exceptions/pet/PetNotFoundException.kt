package com.ciai.vetclinic.service.exceptions.pet

import com.ciai.vetclinic.service.exceptions.NotFoundServiceException

class PetNotFoundException (petId: Long) : NotFoundServiceException("There is no pet with id $petId")