package com.ciai.vetclinic.service.exceptions

open class BadRequestServiceException(msg: String) : RuntimeException(msg)
open class NotFoundServiceException(msg: String) : RuntimeException(msg)
open class ConflictServiceException(msg: String) : RuntimeException(msg)
open class AcceptedServiceException(msg: String) : RuntimeException(msg)
open class ForbiddenServiceException(msg: String) : RuntimeException(msg)