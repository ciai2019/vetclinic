package com.ciai.vetclinic.service.exceptions.slot

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class SlotDoesNotBelongToVetException (slotId: Long, wrongVetId: Long) :
        BadRequestServiceException("Slot $slotId does not belong to vet $wrongVetId")