package com.ciai.vetclinic.service.exceptions.admin

import com.ciai.vetclinic.service.exceptions.NotFoundServiceException

class AdminNotFoundException (adminId: Long) : NotFoundServiceException("There is no admin with id $adminId")