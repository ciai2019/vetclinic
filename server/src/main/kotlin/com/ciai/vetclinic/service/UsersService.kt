package com.ciai.vetclinic.service

import com.ciai.vetclinic.model.user.*
import com.ciai.vetclinic.repository.UsersRepository
import com.ciai.vetclinic.service.exceptions.UserNotFoundServiceException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class UsersService(var users: UsersRepository) {

    private fun findUserDAOByUsername(username: String) =
            users.findByUsername(username).orElseThrow { UserNotFoundServiceException(0, username) }

    private fun findUserDAOById(userId: Long) =
            users.findById(userId).orElseThrow { UserNotFoundServiceException(userId) }


    fun findUserByUsername(username: String): Optional<User> = users.findByUsername(username)

    fun addUser(user: User): Optional<User> {
        val aUser = users.findById(user.id)

        return if (aUser.isPresent)
            Optional.empty()
        else {
            user.password = BCryptPasswordEncoder().encode(user.password)
            Optional.of(users.save(user))
        }
    }
    fun getUserIdByUsername(username: String) : UserIdDTO = UserIdDTO(users.findByUsername(username).get().id)

    fun getUserById(id:Long) : UserDTO = UserDTO(findUserDAOById(id))

    fun getAllEmployees(name: Optional<String>): List<EmployeeDTO> {
        lateinit var employees: List<EmployeeDTO>

        employees = if(name.isPresent) {
            users.getAllEmployeesWithNameLike(name.get()).map { user: User -> EmployeeDTO(user) }
        }else {
            users.getAllEmployees().map { user: User -> EmployeeDTO(user) }
        }

        // Filter for name not empty to exclude root admin
        return employees.filter { employeeDTO: EmployeeDTO -> employeeDTO.name.isNotEmpty() }
    }

    fun editUserInfo(userId: Long, updateUserDTO: UpdateUserDTO): UpdateUserDTO {
        val user = findUserDAOById(userId)
        user.update(updateUserDTO)

        return UpdateUserDTO(users.save(user))
    }

    fun getUserInfo(username: String): UserDTO {
        val user = findUserDAOByUsername(username)
        return UserDTO(user)
    }
}