package com.ciai.vetclinic.service.exceptions.vet.schedule

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class TooManyHoursInWeekException : BadRequestServiceException("Schedule exceeds weekly work hours limit")