package com.ciai.vetclinic.service.exceptions.vet.schedule

import com.ciai.vetclinic.service.exceptions.BadRequestServiceException

class InvalidShiftDurationException :
        BadRequestServiceException("Invalid shift duration, must be divisible by slot duration")