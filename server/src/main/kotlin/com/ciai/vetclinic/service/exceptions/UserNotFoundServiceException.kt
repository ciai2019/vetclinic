package com.ciai.vetclinic.service.exceptions

class UserNotFoundServiceException (userId: Long, username: String = "") :
        NotFoundServiceException("There is no user with id ${if(username != "") username else userId}")