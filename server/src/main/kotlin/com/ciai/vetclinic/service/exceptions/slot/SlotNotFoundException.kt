package com.ciai.vetclinic.service.exceptions.slot

import com.ciai.vetclinic.service.exceptions.NotFoundServiceException

class SlotNotFoundException(slotId: Long) :
        NotFoundServiceException("There is no slot with id $slotId")