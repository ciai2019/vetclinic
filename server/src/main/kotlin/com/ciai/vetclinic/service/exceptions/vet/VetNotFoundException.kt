package com.ciai.vetclinic.service.exceptions.vet

import com.ciai.vetclinic.service.exceptions.NotFoundServiceException

class VetNotFoundException (vetId: Long) : NotFoundServiceException("There is no veterinarian with id $vetId")