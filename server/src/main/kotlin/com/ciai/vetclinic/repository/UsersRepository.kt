package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.user.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface UsersRepository : CrudRepository<User, Long>{
    fun findByUsername(username: String): Optional<User>

    @Query("select u from User u where u.role = com.ciai.vetclinic.model.user.Role.ROLE_ADMIN or u.role = com.ciai.vetclinic.model.user.Role.ROLE_VET")
    fun getAllEmployees(): List<User>

    @Query("select u from User u where u.role = com.ciai.vetclinic.model.user.Role.ROLE_ADMIN or u.role = com.ciai.vetclinic.model.user.Role.ROLE_VET and lower(u.name) like lower(concat('%', :name,'%'))")
    fun getAllEmployeesWithNameLike(name: String): List<User>
}