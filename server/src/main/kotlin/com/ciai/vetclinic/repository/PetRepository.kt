package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.pet.Pet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PetRepository : JpaRepository<Pet, Long> {
    fun findAllByRemovedFalse(): Iterable<Pet>
    fun findAllByRemovedTrue(): Iterable<Pet>
    fun findByIdAndRemovedFalse(id: Long): Optional<Pet>

}