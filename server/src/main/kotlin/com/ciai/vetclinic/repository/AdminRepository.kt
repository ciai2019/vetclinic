package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.user.admin.Admin
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AdminRepository : JpaRepository<Admin, Long>{

    @Query("select a from Admin a where a.isDefault = true")
    fun getDefaultAdmin(): Optional<Admin>
}