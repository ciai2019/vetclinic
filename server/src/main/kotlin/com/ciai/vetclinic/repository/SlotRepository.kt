package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.timeslot.TimeSlot
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SlotRepository : JpaRepository<TimeSlot, Long>{

    /*
    @Query("select s from TimeSlot s inner join Vet v on s.vetId = v.id where s.vetId = :vetId and s.id = :slotId")
    fun getSlotOfVetById(vetId: Long, slotId: Long): Optional<TimeSlot>

     */

    fun getByIdAndVetId(id: Long, vet_id: Long): Optional<TimeSlot>

    fun getAllByAppointmentIsNull(): List<TimeSlot>

    fun getAllByAppointmentIsNotNull(): List<TimeSlot>
}