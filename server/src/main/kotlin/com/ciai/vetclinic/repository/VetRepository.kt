package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.user.vet.Vet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface VetRepository : JpaRepository<Vet, Long>{

    @Query("select v from Vet v where v.id = :vetId and v.frozenAccount = false")
    override fun findById(vetId: Long): Optional<Vet>
}