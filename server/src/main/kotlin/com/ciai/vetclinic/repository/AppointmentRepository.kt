package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.appointment.Appointment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AppointmentRepository : JpaRepository<Appointment, Long> {

    fun getAppointmentsByVetIdAndCompletedTrue(vet_id: Long): List<Appointment>

    fun getAppointmentsByVetIdAndCompletedFalse(vet_id: Long): List<Appointment>

    //findById must be used in all application (must list pets that havent been removed)
    @Query("select a from Appointment a inner join a.pet p where a.id = :appointmentId and p.removed = false")
    override fun findById(appointmentId: Long): Optional<Appointment>

    //findAll must be used in all application (must list pets that havent been removed)
    @Query("select a from Appointment a inner join a.pet p where p.removed = false")
    override fun findAll(): List<Appointment>
}