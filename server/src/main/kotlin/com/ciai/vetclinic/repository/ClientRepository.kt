package com.ciai.vetclinic.repository

import com.ciai.vetclinic.model.user.client.Client
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ClientRepository : JpaRepository<Client, Long> {
    fun findClientByUsername(username: String): Optional<Client>
}