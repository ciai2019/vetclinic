package com.ciai.vetclinic.config

import com.ciai.vetclinic.repository.UsersRepository
import com.ciai.vetclinic.service.AdminService
import com.ciai.vetclinic.service.UsersService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.web.bind.annotation.CrossOrigin

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@EnableWebSecurity
@CrossOrigin
class SecurityConfig(
        val customUserDetailsService: CustomUserDetailsService,
        val users: UsersService,
        val userRep: UsersRepository
) : WebSecurityConfigurerAdapter() {

    @Autowired
    lateinit var adminService: AdminService

    override fun configure(auth: AuthenticationManagerBuilder) {
        adminService.createDefaultAdminIfNotPresent()

        for (user in userRep.findAll()) {
            val role = user.role.name.removePrefix("ROLE_")

            auth.inMemoryAuthentication()
                    .withUser(user.username)
                    .password(BCryptPasswordEncoder().encode(user.password)).roles(role)
                    .and()
                    .passwordEncoder(BCryptPasswordEncoder())
                    .and()
                    .userDetailsService(customUserDetailsService)
                    .passwordEncoder(BCryptPasswordEncoder())
        }


    }

    override fun configure(http: HttpSecurity) {
        http.csrf().disable().cors().and() // for now, we can disable cross site request forgery protection
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(UserPasswordAuthenticationFilterToJWT("/login", super.authenticationManagerBean()),
                        BasicAuthenticationFilter::class.java)
                .addFilterBefore(JWTAuthenticationFilter(users),
                        BasicAuthenticationFilter::class.java)

    }

    override fun configure(web: WebSecurity?) {
        web!!.ignoring()
                .antMatchers("/v2/**")
                .antMatchers("/webjars/**")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/swagger-ui.html")
                .antMatchers(HttpMethod.GET,"/api/employees")
    }



}