package com.ciai.vetclinic.config

import com.ciai.vetclinic.model.user.Role
import com.ciai.vetclinic.model.user.User
import com.ciai.vetclinic.service.UsersService
import com.ciai.vetclinic.service.exceptions.UserNotFoundServiceException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

class CustomUserDetails(
        private val username: String,
        private val password: String,
        //val role: Role,
        private val authorities: MutableCollection<out GrantedAuthority>

) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = authorities

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = username

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = password

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true


}


@Service
class CustomUserDetailsService(
        val users: UsersService
) : UserDetailsService {

    override fun loadUserByUsername(username: String?): CustomUserDetails {

        username?.let {
            val userDAO = users.findUserByUsername(it)
            if (userDAO.isPresent) {
                val authorities: MutableList<GrantedAuthority> = mutableListOf()

                authorities.add(SimpleGrantedAuthority(userDAO.get().role.name))

                return CustomUserDetails(userDAO.get().username, userDAO.get().password, authorities)
            } else
                throw UsernameNotFoundException(username)
        }
        throw UsernameNotFoundException(username)
    }

    fun getUserDAO(userDetails: CustomUserDetails): User =
            users.findUserByUsername(userDetails.username).orElseThrow { UserNotFoundServiceException(-1, userDetails.username) }
}