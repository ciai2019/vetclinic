package com.ciai.vetclinic.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
class Swagger2Config {

    @Bean
    fun api(): Docket =
            Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.ciai.vetclinic"))
                    .paths(PathSelectors.any())
                    .build().apiInfo(apiEndpointsInfo())

    fun apiEndpointsInfo(): ApiInfo =
            ApiInfoBuilder()
                    .title("VetClinic Spring Boot REST API")
                    .description("VetClinic REST API for IADI course project")
                    .contact(Contact("André Rodrigues, João Teixeira and Francisco António", "", "ao.rodrigues@campus.fct.unl.pt, ja.teixeira@campus.fct.unl.pt, fm.antonio@campus.fct.unl.pt"))
                    .license("Apache 2.0")
                    .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                    .version("1.0.0")
                    .build()
}