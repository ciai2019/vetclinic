package com.ciai.vetclinic.controller

import com.ciai.vetclinic.model.user.vet.NewVetDTO
import com.ciai.vetclinic.model.user.vet.Vet
import com.ciai.vetclinic.model.user.vet.VetDTO
import com.ciai.vetclinic.service.VetService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "user", password = "password", roles = ["USER"])
class VetControllerTest {
    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var vets: VetService

    companion object{
        val mapper = ObjectMapper().registerModule(KotlinModule())

        val vetsURL = "/api/vets"

    }

    @Test
    fun `Test POST one Vet`(){
        val newDrBaitacaDTO = NewVetDTO("Dr Baitaça", "", "baitaca@mail.com", "42069", "Rua da Baitaça", "baitaca123", "pass")
        val drBaitaca = Vet(newDrBaitacaDTO)
        val drBaitacaDTO = VetDTO(drBaitaca)

        val drBaitacaJSON = mapper.writeValueAsString(newDrBaitacaDTO)

        Mockito.`when`(vets.hireNewVet(newDrBaitacaDTO))
                .then { assertThat(it.getArgument(0), equalTo(drBaitacaDTO))}

        mvc.perform(post(vetsURL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(drBaitacaJSON))
                .andExpect(status().isOk)
    }
}