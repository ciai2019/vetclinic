package com.ciai.vetclinic.controller

import com.ciai.vetclinic.model.user.client.ClientDTO
import com.ciai.vetclinic.model.user.client.NewClientDTO
import com.ciai.vetclinic.service.ClientService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import javassist.NotFoundException
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "user", password = "password", roles = ["USER"])
class ClientControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var clients: ClientService
    lateinit var AndreDTO: ClientDTO
    lateinit var JoaoDTO: ClientDTO

    companion object {
        // To avoid all annotations JsonProperties in data classes
        // see: https://github.com/FasterXML/jackson-module-kotlin
        // see: https://discuss.kotlinlang.org/t/data-class-and-jackson-annotation-conflict/397/6
        val mapper = ObjectMapper().registerModule(KotlinModule())
        val Andre = NewClientDTO("Andre", "none", "Andre@gmail.com", "9453459345", "Almada", "Andre", "password")
        val Joao = NewClientDTO("Joao", "none", "Joao@gmail.com", "9453459434234", "Monte Kapta", "Joao", "password")
        var clientsDAO = listOf<ClientDTO>()


        val clientURL = "/clients"
    }

    @Test
    fun `Test GET all pets`() {
        AndreDTO = clients.createNewClient(Andre)
        JoaoDTO = clients.createNewClient(Joao)
        clientsDAO = ArrayList(listOf(AndreDTO, JoaoDTO))
        Mockito.`when`(clients.getAllClients()).thenReturn(clientsDAO)

        val result = mvc.perform(MockMvcRequestBuilders.get(clientURL))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<ClientDTO>>(responseString)
        Assert.assertThat(responseDTO.toList(), CoreMatchers.equalTo(clientsDAO.toList()))
    }

    @Test
    fun `Test Get One Pet`() {
        Mockito.`when`(clients.getClient(1)).thenReturn(AndreDTO)

        val result = mvc.perform(MockMvcRequestBuilders.get("$clientURL/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<ClientDTO>(responseString)
        Assert.assertThat(responseDTO, CoreMatchers.equalTo(clientsDAO[0]))
    }

    @Test
    fun `Test GET One Pet (Not Found)`() {
        Mockito.`when`(clients.getClient(2)).thenThrow(NotFoundException("not found"))

        mvc.perform(MockMvcRequestBuilders.get("$clientURL/2"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)
    }

    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)

    /*@Test
    fun `Test POST One Pet`() {
        val louro = PetDTO(0, "louro", "Papagaio")
        val louroDAO = PetDAO(louro.id, louro.name, louro.species, emptyList())

        val louroJSON = mapper.writeValueAsString(louro)

        Mockito.`when`(clients.addNewPet(nonNullAny(PetDAO::class.java)))
                .then { Assert.assertThat(it.getArgument(0), CoreMatchers.equalTo(louroDAO)); it.getArgument(0) }

        mvc.perform(MockMvcRequestBuilders.post(clientURL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(louroJSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `Test checking appointments`() {
        val louro = PetDAO(1, "louro", "Papagaio", emptyList())
        val apt = AppointmentDAO(2, Date(), "consulta", louro)
        louro.appointments = listOf(apt)

        Mockito.`when`(clients.appointmentsOfPet(1)).thenReturn(listOf(apt))

        //val result =
        mvc.perform(MockMvcRequestBuilders.get("$clientURL/1/appointments"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        /* TODO: need to compare with result
        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<ClientDTO>>(responseString)
        assertThat(responseDTO, equalTo(clientsDAO))
        */
    }

    @Test
    fun `Test checking appointments of non pet`() {
        Mockito.`when`(clients.appointmentsOfPet(1))
                .thenThrow(NotFoundException("not found"))

        mvc.perform(MockMvcRequestBuilders.get("$clientURL/1/appointments"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)
    }

    @Test
    fun `Test adding an appointment to a pet`() {
        val louro = PetDAO(1, "louro", "Papagaio", emptyList())
        val apt = AppointmentDTO(0, Date(), "consulta")
        val aptDAO = AppointmentDAO(apt, louro)
        louro.appointments = listOf(aptDAO)

        val aptJSON = mapper.writeValueAsString(apt)

        Mockito.`when`(clients.newAppointment(nonNullAny(AppointmentDAO::class.java)))
                .then { Assert.assertThat(it.getArgument(0), CoreMatchers.equalTo(aptDAO)); it.getArgument(0) }

        Mockito.`when`(clients.getClient(1)).thenReturn(louro)

        mvc.perform(MockMvcRequestBuilders.post("$clientURL/1/appointments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(aptJSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `Bad request on id not 0`() {
        val louro = PetDAO(1, "louro", "Papagaio", emptyList())
        val apt = AppointmentDTO(2, Date(), "consulta")
        val aptDAO = AppointmentDAO(apt, louro)
        louro.appointments = listOf(aptDAO)

        val aptJSON = mapper.writeValueAsString(apt)

        Mockito.`when`(clients.newAppointment(nonNullAny(AppointmentDAO::class.java)))
                .thenThrow(PreconditionFailedException("id 0"))

        Mockito.`when`(clients.getClient(1)).thenReturn(louro)

        mvc.perform(MockMvcRequestBuilders.post("$clientURL/1/appointments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(aptJSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError)

    }
} */
}