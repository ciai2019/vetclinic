-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: vetclinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pet`
--

DROP TABLE IF EXISTS `pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pet` (
  `id` bigint(20) NOT NULL,
  `age` int(11) NOT NULL,
  `medical_record` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `physical_description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `removed` bit(1) NOT NULL,
  `species` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5ecbt77k618rp8yurg5dxppfb` (`owner_id`),
  CONSTRAINT `FK5ecbt77k618rp8yurg5dxppfb` FOREIGN KEY (`owner_id`) REFERENCES `client` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet`
--

LOCK TABLES `pet` WRITE;
/*!40000 ALTER TABLE `pet` DISABLE KEYS */;
INSERT INTO `pet` VALUES (46,3,'','Óscar','much wow','very doge','https://images-na.ssl-images-amazon.com/images/I/81-yKbVND-L._SY355_.png',_binary '\0','Cão',45),(50,2,'','Alho','','Stylish AF','https://i.pinimg.com/originals/b1/01/e4/b101e481ee632734ff6b854587687a6b.jpg',_binary '\0','Alpaca',45),(51,69,'','Pringles','','','https://i.pinimg.com/originals/39/1f/88/391f881bb2885a4c8fa788975d628e39.jpg',_binary '\0','Not a potato',45),(52,420,'','Faustino','','500 server error','https://sadanduseless.b-cdn.net/wp-content/uploads/2019/09/funny-dog-snapchats6.jpg',_binary '\0','Sinceramente nem sei',45),(53,18,'','Zeca','','Fine AF','https://cdn3.cmjornal.pt/images/2017-06/img_432x244$2017_06_28_15_56_01_642664.jpg',_binary '\0','Prima do Açores',45),(54,23,'','zepaasdf','','','',_binary '\0','asd',45),(55,12,'','sdasdasd','','','',_binary '\0','sadads',45),(62,0,'','test','','','',_binary '\0','',45);
/*!40000 ALTER TABLE `pet` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-06 22:15:12
